package org.dudullle.pixels.evolved.alliances;

import net.milkbowl.vault.economy.EconomyResponse;
import org.dudullle.pixels.evolved.survival.modo.ModoCommand;
import org.dudullle.pixels.evolved.survival.money.MoneyManager;
import org.dudullle.pixels.evolved.survival.alliances.PeAlliance;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlag;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PeAllianceFlag.class, MoneyManager.class})
public class PeAllianceTest {

    @Mock
    PePlayer creator;

    @Mock
    PePlayer player1;

    @Mock
    PePlayer player2;

    @Mock
    PeAllianceFlag flag1;

    @Mock
    MoneyManager moneyManager;

    @Before
    public void testSetup() throws Exception {
        when(creator.getUuid()).thenReturn(UUID.randomUUID());
        when(player1.getUuid()).thenReturn(UUID.randomUUID());
        when(player2.getUuid()).thenReturn(UUID.randomUUID());
        when(flag1.getxLocation()).thenReturn(1);
        when(flag1.getzLocation()).thenReturn(1);
        Field f = MoneyManager.class.getDeclaredField("INSTANCE");
        setFinalStatic(f, moneyManager);
    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }

    @Test
    public void testAllianceAddMemberAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);
        //Test ajout 2
        message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.ERROR_ALREADY_MEMBER, message);
    }

    @Test
    public void testAllicanceAddMemberNonAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, player2);
        assertEquals(ReturnMessage.ERROR_NOT_ADMIN, message);
    }

    @Test
    public void testPromoteAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player1, creator);
        assertEquals(ReturnMessage.ERROR_ALREADY_ADMIN, message);
    }

    @Test
    public void testPromoteNonAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player1, player1);
        assertEquals(ReturnMessage.ERROR_NOT_ADMIN, message);
    }

    @Test
    public void testPromoteNonMember(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player2, player1);
        assertEquals(ReturnMessage.ERROR_NOT_IN_ALLIANCE, message);

        message = peAlliance.promote(player1, player2);
        assertEquals(ReturnMessage.ERROR_NOT_IN_ALLIANCE, message);
    }

    @Test
    public void testPromoteNonMemberAndAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player2, creator);
        assertEquals(ReturnMessage.ERROR_NOT_IN_ALLIANCE, message);
    }

    @Test
    public void testRemovePlayerAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.removePlayer(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.removePlayer(player1, creator);
        assertEquals(ReturnMessage.ERROR_NOT_IN_ALLIANCE, message);
    }

    @Test
    public void testRemovePlayerNonAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.removePlayer(player1, player1);
        assertEquals(ReturnMessage.ERROR_NOT_ADMIN, message);
    }

    @Test
    public void testRemovePlayerNonAdminNonMember(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.removePlayer(player1, player2);
        assertEquals(ReturnMessage.ERROR_NOT_IN_ALLIANCE, message);

        message = peAlliance.removePlayer(player2, creator);
        assertEquals(ReturnMessage.ERROR_NOT_IN_ALLIANCE, message);
    }

    @Test
    public void testDemotePlayerAdmin() {
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.demote(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.demote(player1, creator);
        assertEquals(ReturnMessage.ERROR_NOT_ADMIN, message);
    }

    @Test
    public void testDemotePlayerNonAdmin(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.addMember(player2, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.demote(player1, player2);
        assertEquals(ReturnMessage.ERROR_NOT_ADMIN, message);
    }

    @Test
    public void testDemotePlayerNonMember(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.promote(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.demote(player1, player2);
        assertEquals(ReturnMessage.ERROR_NOT_IN_ALLIANCE, message);
    }

    @Test
    public void testGetFlagAt(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addFlag(flag1);
        assertEquals(ReturnMessage.SUCCESS, message);

        PeAllianceFlag returnFlag = peAlliance.getFlagAt(0,1);
        assertNull( returnFlag);

        returnFlag = peAlliance.getFlagAt(1,1);
        assertEquals(flag1, returnFlag);
    }

    @Test
    public void testRemoveFlagAdminHasMoney() throws Exception {
        EconomyResponse er = new EconomyResponse(100,100, EconomyResponse.ResponseType.SUCCESS, "");
        when(moneyManager.withdrawPlayer(Mockito.anyString(), Mockito.anyDouble())).thenReturn(er);

        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addFlag(flag1);
        assertEquals(ReturnMessage.SUCCESS, message);
/*
        message = peAlliance.removeFlag(1,1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.removeFlag(1,1, creator);
        assertEquals(ReturnMessage.FLAG_NOT_FOUND, message);

 */
    }

    @Test
    public void testRemoveFlagAdminHasntMoney() throws Exception {
        EconomyResponse er = new EconomyResponse(100,100, EconomyResponse.ResponseType.FAILURE, "");
        when(moneyManager.withdrawPlayer(Mockito.anyString(), Mockito.anyDouble())).thenReturn(er);

        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addFlag(flag1);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.removeFlag(1,1, creator);
        assertEquals(ReturnMessage.NO_ENOUGH_MONEY, message);
    }

    @Test
    public void testRemoveFlagNotAdmin() throws Exception {
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        ReturnMessage message = peAlliance.addMember(player1, creator);
        assertEquals(ReturnMessage.SUCCESS, message);
        message = peAlliance.addFlag(flag1);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.removeFlag(1,1, player1);
        assertEquals(ReturnMessage.ERROR_NOT_ADMIN, message);
    }

    @Test
    public void testDestroyFlag(){
        PeAlliance peAlliance = new PeAlliance(creator, "ALLIANCE");
        PeAlliance peAllianceEnnemy = new PeAlliance(creator, "ALLIANCE_ENN");

        ReturnMessage message = peAlliance.addFlag(flag1);
        assertEquals(ReturnMessage.SUCCESS, message);

        message = peAlliance.destroyFlag(1,1, peAlliance);
        assertEquals(ReturnMessage.SELF_DESTRUCTION_NOT_AVAILABLE, message);

        message = peAlliance.destroyFlag(0,1, peAllianceEnnemy);
        assertEquals(ReturnMessage.FLAG_NOT_FOUND, message);

        message = peAlliance.destroyFlag(1,1, peAllianceEnnemy);
        assertEquals(ReturnMessage.FLAG_DESTROY_SUCCESS, message);
    }
}
