package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.utils.TeleportUtils;

public class SpawnCommand extends AbstractCommand {
    public static final String COMMAND_NAME = "spawn";
    public static final SpawnCommand INSTANCE = new SpawnCommand();

    private SpawnCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Location spawn = player.getWorld().getSpawnLocation();
            if (((PluginMain) PluginMain.getInstance()).isLobby()) {
                player.teleportAsync(spawn);
                player.playSound(spawn, Sound.ENTITY_FOX_TELEPORT, 100, 0);
            } else {
                TeleportUtils.INSTANCE.teleportWithDelay(new AlliancePointOfInterest(spawn), null, player);
            }
        } else {
            sender.sendMessage("La console ne peut se téléporter.");
        }
        return true;
    }
}
