package org.dudullle.pixels.evolved.survival.listeners.customevents;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.dudullle.pixels.evolved.survival.alliances.PeAlliance;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

public class EndOfTimeEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final PePlayer pePlayer;
    private final PeAlliance alliance;
    private final Player player;
    private ActionType actionType;

    public EndOfTimeEvent(ActionType actionType, PePlayer pePlayer, PeAlliance peAlliance, Player player) {
        this.actionType = actionType;
        this.pePlayer = pePlayer;
        this.alliance = peAlliance;
        this.player = player;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public PePlayer getPePlayer() {
        return pePlayer;
    }

    public PeAlliance getAlliance() {
        return alliance;
    }

    public Player getPlayer() {
        return player;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public enum ActionType {
        DO_CLAIM_TICK,
    }
}
