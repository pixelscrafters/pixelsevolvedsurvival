package org.dudullle.pixels.evolved.survival.players;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.UUID;

public class PePlayerData {
    public UUID pePlayerUUID;
    public PePlayerDataType dataType;
    public String data;
    public long saveDate;

    public PePlayerData(UUID pePlayerUUID, PePlayerDataType dataType, String data, long saveDate) {
        this.pePlayerUUID = pePlayerUUID;
        this.dataType = dataType;
        this.data = data;
        this.saveDate = saveDate;
    }

    public static PePlayerData fromJson(String json) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, PePlayerData.class);
    }
}
