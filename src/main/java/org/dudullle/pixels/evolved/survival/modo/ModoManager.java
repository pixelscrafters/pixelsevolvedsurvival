package org.dudullle.pixels.evolved.survival.modo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.dudullle.pixels.evolved.survival.utils.FileUtils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class ModoManager {
    public static final String FILE_LOCATION = "modo.json";
    public static final String CONFIG_FILE_LOCATION = "modoConfig.json";
    public static final String KICK_MESSAGE =
            "§5----------------------------------\n" +
                    "§cVous avez été expulsé du serveur !\n" +
                    "§5----------------------------------\n" +
                    "§c%TITLE% : §7 %DESC%\n";

    public static final String BAN_MESSAGE =
            "§5----------------------------------\n" +
                    "§cVous avez été expulsé du serveur !\n" +
                    "§5----------------------------------\n" +
                    "§c%TITLE% : §7 %DESC%\n" +
                    "§5----------------------------------\n" +
                    "§cBan jusqu'au : %BAN_END%";
    public static final ModoManager INSTANCE = new ModoManager();
    private List<ModoPlayer> players;
    private ModoConfig configuration;

    private ModoManager() {

    }

    public void loadService() {
        ///Lecture du fichier JSON
        List<String> lines = FileUtils.INSTANCE.readAllText(FILE_LOCATION);
        if (lines != null) {
            String line = String.join(" ", lines);
            Gson gson = new GsonBuilder().create();
            Type listOfMyClassObject = new TypeToken<ArrayList<ModoPlayer>>() {
            }.getType();
            players = gson.fromJson(line, listOfMyClassObject);
        }
        if (players == null) {
            players = new ArrayList<>();
        }

        lines = FileUtils.INSTANCE.readAllText(CONFIG_FILE_LOCATION);
        if (lines != null) {
            String line = String.join(" ", lines);
            Gson gson = new GsonBuilder().create();
            configuration = gson.fromJson(line, ModoConfig.class);
        } else {
            configuration = new ModoConfig();
        }

    }

    public void shutdownService() {
        //Sauvegarde
        Gson gson = new GsonBuilder().create();
        List<String> lines = new ArrayList<>();
        lines.add(gson.toJson(players));
        FileUtils.INSTANCE.saveFile(FILE_LOCATION, lines);

        lines = new ArrayList<>();
        lines.add(gson.toJson(configuration));
        FileUtils.INSTANCE.saveFile(CONFIG_FILE_LOCATION, lines);
    }

    public List<ModoPlayer> findPlayersByIp(String ip) {
        List<ModoPlayer> playersCopy = getPlayers();
        return playersCopy.stream().filter(p -> p.containsIp(ip)).collect(Collectors.toList());
    }

    public ModoPlayer getPlayer(UUID uuid) {
        List<ModoPlayer> playersCopy = getPlayers();
        return playersCopy.stream().filter(p -> p.getUuid().equals(uuid)).findFirst().orElse(null);
    }

    public ModoPlayer getPlayer(String name) {
        List<ModoPlayer> playersCopy = getPlayers();
        return playersCopy.stream().filter(p -> p.getName().equals(name)).findFirst().orElse(null);
    }

    public List<ModoPlayer> getPlayers() {
        if (players == null) {
            players = new ArrayList<>();
        }
        return new ArrayList<>(players);
    }

    public boolean handleInsults(ModoPlayer player, String message, ProxiedPlayer proxiedPlayer) {
        boolean containsInsult = false;
        List<String> insults = configuration.getInsultes();
        String[] words = message.toLowerCase().split(" ");

        for (String x : words) {
            if (insults.contains(x)) {
                //On kick
                containsInsult = true;
                ModoInsult insult = new ModoInsult(x, System.currentTimeMillis());
                player.registerInsult(insult);
            }
        }
        this.kickPlayer(ModoKick.ModoKickReason.INSULT, "Les insultes sont interdites.", proxiedPlayer, null);
        return containsInsult;
    }

    public void registerNewPlayer(UUID player, String playerName, String ip) {
        ModoPlayer modoPlayer = new ModoPlayer(player, playerName);
        modoPlayer.registerIp(ip);
        addPlayer(modoPlayer);
    }

    private synchronized void addPlayer(ModoPlayer modoPlayer) {
        this.players.add(modoPlayer);
    }

    public void kickPlayer(ModoKick.ModoKickReason reason, String description, ProxiedPlayer kicked, ProxiedPlayer kicker) {
        ModoPlayer modoPlayer = getPlayer(kicked.getUniqueId());
        ModoKick modoKick = new ModoKick(reason, kicker.getUniqueId(), description, System.currentTimeMillis());

        if (modoPlayer != null) {
            modoPlayer.registerKick(modoKick);
        }
        String desc = description != null ? description : "Informations supplémentaires à demander.";
        String message = KICK_MESSAGE.replace("%DESC%", desc).replace("%TITLE%", reason.title);
        kicked.disconnect(new TextComponent(message));
        ProxyServer.getInstance().broadcast(new TextComponent("§cLe joueur §d" + kicked.getName() + " §ca été expulsé du serveur pour la raison suivante §d" + reason.title));
    }

    public void banPlayer(ModoBan.ModoBanReason reason, String description, long delay, ProxiedPlayer banned, ProxiedPlayer banner) {
        ModoPlayer modoPlayer = getPlayer(banned.getUniqueId());
        ModoBan modoBan = new ModoBan(reason, System.currentTimeMillis() + delay, System.currentTimeMillis(),
                banner.getUniqueId(), description);

        if (modoPlayer != null) {
            modoPlayer.registerBan(modoBan);
        }
        String desc = description != null ? description : "Informations supplémentaires à demander.";
        DateFormat df = new SimpleDateFormat("EEEE, d MMM yyyy 'à' HH:mm:ss", Locale.FRENCH);
        String message = BAN_MESSAGE.replace("%DESC%", desc).replace("%TITLE%", reason.title).replace("%BAN_END%", df.format(new Date(modoBan.getBanExpiration())));
        banned.disconnect(new TextComponent(message));
        ProxyServer.getInstance().broadcast(new TextComponent("§cLe joueur §d" + banned.getName() + " §ca été ban du serveur pour la raison suivante §d" + reason.title));
    }

    public void banPlayer(ModoBan.ModoBanReason reason, String description, long delay, String bannedName, ProxiedPlayer banner) {
        ModoPlayer modoPlayer = getPlayer(bannedName);
        ModoBan modoBan = new ModoBan(reason, System.currentTimeMillis() + delay, System.currentTimeMillis(),
                banner.getUniqueId(), description);

        if (modoPlayer != null) {
            modoPlayer.registerBan(modoBan);
        }
        String desc = description != null ? description : "Informations supplémentaires à demander.";
        DateFormat df = new SimpleDateFormat("EEEE, d MMM yyyy 'à' HH:mm:ss", Locale.FRENCH);
        String message = BAN_MESSAGE.replace("%DESC%", desc).replace("%TITLE%", reason.title).replace("%BAN_END%", df.format(new Date(modoBan.getBanExpiration())));
        ProxiedPlayer bannedPlayer = ProxyServer.getInstance().getPlayer(bannedName);
        if (bannedPlayer != null) {
            bannedPlayer.disconnect(new TextComponent(message));
        }
        ProxyServer.getInstance().broadcast(new TextComponent("§cLe joueur §d" + modoPlayer.getName() + " §ca été ban du serveur pour la raison suivante §d" + reason.title));
    }

    public void unban(String name, ProxiedPlayer unbanner) {
        List<ModoPlayer> players = getPlayers();
        ModoPlayer modoPlayer = players.stream().filter(p -> p.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
        if (modoPlayer == null) {
            unbanner.sendMessage(new TextComponent("§cLe joueur spécifié n'a pas été trouvé."));
        } else {
            modoPlayer.getBans().stream().filter(x -> x.getBanExpiration() > System.currentTimeMillis()).forEach(ModoBan::setBanExpirationOver);
            unbanner.sendMessage(new TextComponent("§aLe joueur a été déban !"));
        }
    }

    public boolean isBanned(ModoPlayer modoPlayer) {
        return modoPlayer.getBans().stream().anyMatch(x -> x.getBanExpiration() > System.currentTimeMillis());
    }

    public String getBanMessage(ModoPlayer modoPlayer) {
        ModoBan modoBan = modoPlayer.getBans().stream().filter(x -> x.getBanExpiration() > System.currentTimeMillis()).findFirst().orElse(null);
        if (modoBan != null) {
            String desc = modoBan.getBanDescription() != null ? modoBan.getBanDescription() : "Informations supplémentaires à demander.";
            DateFormat df = new SimpleDateFormat("EEEE, d MMM yyyy 'à' HH:mm:ss", Locale.FRENCH);
            return BAN_MESSAGE.replace("%DESC%", desc).replace("%TITLE%", modoBan.getBanReason().title).replace("%BAN_END%", df.format(new Date(modoBan.getBanExpiration())));
        } else {
            return "§cErreur technique. Contactez le staff !";
        }
    }
}
