package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.PluginMain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VanishCommand extends AbstractCommand {
    public static final String COMMAND_NAME = "vanish";
    public static final VanishCommand INSTANCE = new VanishCommand();
    private final List<UUID> vanishedUUIDs = new ArrayList<>();

    private VanishCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 1) {
                if (player.hasPermission("pe.vanish.others")) {
                    String playerName = args[0];
                    player = Bukkit.getPlayer(playerName);
                } else {
                    player.sendMessage("§cVous ne pouvez pas vanish d'autres joueurs.");
                }
            }
            if (player == null || !player.isOnline()) {
                sender.sendMessage("§cLe joueur spécifié n'a pas été trouvé.");
            } else if (vanishedUUIDs.contains(player.getUniqueId())) {
                vanishedUUIDs.remove(player.getUniqueId());
                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.showPlayer(PluginMain.getInstance(), player);
                }
                player.sendMessage("§eVous êtes visible.");
            } else {
                vanishedUUIDs.add(player.getUniqueId());
                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.hidePlayer(PluginMain.getInstance(), player);
                }
                player.sendMessage("§eVous êtes invisible.");
            }
        } else {
            sender.sendMessage("La console ne peut pas faire cette commande.");
        }
        return true;
    }
}
