package org.dudullle.pixels.evolved.survival.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

public class TeleportUtils {
    public static TeleportUtils INSTANCE = new TeleportUtils();

    private TeleportUtils() {

    }

    public void teleport(AlliancePointOfInterest location, PePlayer pePlayer, Player p, Location initialLocation) {
        if (initialLocation != null && !initialLocation.equals(p.getLocation())) {
            p.sendMessage("§cTéléportation annulée. Vous avez bougé :(");
        } else if (Bukkit.getWorld(location.getWorldName()) == null) {
            p.sendMessage("§cVous devez être sur le serveur " + location.getWorldName());
        } else {
            p.teleportAsync(location.toLocation());
            p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 100, 0);
        }
    }

    public void teleportWithDelay(AlliancePointOfInterest location, PePlayer pePlayer, Player playerToTeleport) {
        if (playerToTeleport.hasPermission("pe.tp.instant")) {
            teleport(location, pePlayer, playerToTeleport, null);
        } else {
            playerToTeleport.sendMessage("§dTéléportation dans 3 secondes. Ne bougez pas !");
            Location initialLocation = playerToTeleport.getLocation();
            Bukkit.getScheduler().runTaskLater(PluginMain.getInstance(), () -> {
                teleport(location, pePlayer, playerToTeleport, initialLocation);
            }, 20L * 3);
        }
    }
}
