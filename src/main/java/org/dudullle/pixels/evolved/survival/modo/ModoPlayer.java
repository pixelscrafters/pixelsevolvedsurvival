package org.dudullle.pixels.evolved.survival.modo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ModoPlayer {
    private UUID uuid;
    private final String name;
    private final List<ModoMail> mails;
    private List<ModoPlayerIp> modoPlayerIps;
    private List<ModoBan> bans;
    private List<ModoKick> kicks;
    private List<ModoInsult> insults;

    public ModoPlayer(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
        this.mails = new ArrayList<>();
        this.modoPlayerIps = new ArrayList<>();
        this.bans = new ArrayList<>();
        this.kicks = new ArrayList<>();
        this.insults = new ArrayList<>();
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public List<ModoMail> getMails() {
        return mails;
    }

    public List<ModoPlayerIp> getModoPlayerIps() {
        return new ArrayList<>(modoPlayerIps);
    }

    public Boolean containsIp(String ip) {
        if (this.modoPlayerIps == null) {
            modoPlayerIps = new ArrayList<>();
            return false;
        }
        return getModoPlayerIps().stream().filter(x -> x.getIp().equalsIgnoreCase(ip)).parallel().count() > 0;
    }

    public List<ModoBan> getBans() {
        if (this.bans == null) {
            bans = new ArrayList<>();
        }
        return bans;
    }

    public List<ModoKick> getKicks() {
        if (this.kicks == null) {
            kicks = new ArrayList<>();
        }
        return kicks;
    }

    public List<ModoInsult> getInsults() {
        if (this.insults == null) {
            insults = new ArrayList<>();
        }
        return insults;
    }

    public void registerInsult(ModoInsult insult) {
        if (this.insults == null) {
            insults = new ArrayList<>();
        }
        this.insults.add(insult);
    }

    public void registerKick(ModoKick modoKick) {
        if (this.kicks == null) {
            kicks = new ArrayList<>();
        }
        this.kicks.add(modoKick);
    }

    public void registerBan(ModoBan modoBan) {
        if (this.bans == null) {
            bans = new ArrayList<>();
        }
        this.bans.add(modoBan);
    }

    public void registerIp(String ip) {
        if (this.modoPlayerIps == null) {
            this.modoPlayerIps = new ArrayList<>();
        }
        if (!containsIp(ip)) {
            ModoPlayerIp playerIp = new ModoPlayerIp(ip, System.currentTimeMillis());
            this.modoPlayerIps.add(playerIp);
        }
    }
}
