package org.dudullle.pixels.evolved.survival.money;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

import java.text.DecimalFormat;
import java.util.List;
import java.util.UUID;

public class MoneyManager implements Economy {
    public static final MoneyManager INSTANCE = new MoneyManager();

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return "PixelsEvolved Survival";
    }

    @Override
    public boolean hasBankSupport() {
        return true;
    }

    @Override
    public int fractionalDigits() {
        return 0;
    }

    @Override
    public String format(double amount) {
        DecimalFormat dc = new DecimalFormat();
        return dc.format(amount);
    }

    @Override
    public String currencyNamePlural() {
        return "Pixels";
    }

    @Override
    public String currencyNameSingular() {
        return "Pixel";
    }

    @Override
    public boolean hasAccount(String playerName) {
        return PlayersManager.INSTANCE.getPePlayer(UUID.fromString(playerName)) != null;
    }

    @Override
    public boolean hasAccount(OfflinePlayer player) {
        return hasAccount(player.getUniqueId().toString());
    }

    @Override
    public boolean hasAccount(String playerName, String worldName) {
        return hasAccount(playerName);
    }

    @Override
    public boolean hasAccount(OfflinePlayer player, String worldName) {
        return hasAccount(player);
    }

    @Override
    public double getBalance(String playerName) {
        PePlayer player = PlayersManager.INSTANCE.getPePlayer(UUID.fromString(playerName));
        if (player != null) {
            return player.getMoney();
        }
        return 0d;
    }

    @Override
    public double getBalance(OfflinePlayer player) {
        return getBalance(player.getUniqueId().toString());
    }

    @Override
    public double getBalance(String playerName, String world) {
        return getBalance(playerName);
    }

    @Override
    public double getBalance(OfflinePlayer player, String world) {
        return getBalance(player.getUniqueId().toString());
    }

    @Override
    public boolean has(String playerName, double amount) {
        Double pAm = getBalance(playerName);
        return pAm >= amount;
    }

    @Override
    public boolean has(OfflinePlayer player, double amount) {
        return has(player.getUniqueId().toString(), amount);
    }

    @Override
    public boolean has(String playerName, String worldName, double amount) {
        return has(playerName, amount);
    }

    @Override
    public boolean has(OfflinePlayer player, String worldName, double amount) {
        return has(player.getUniqueId().toString(), amount);
    }

    @Override
    public EconomyResponse withdrawPlayer(String playerName, double amount) {
        Double pAm = getBalance(playerName);
        if (has(playerName, amount)) {
            EconomyResponse er = new EconomyResponse(amount, pAm, EconomyResponse.ResponseType.SUCCESS, "OK");
            PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(UUID.fromString(playerName));
            pePlayer.getTransactions().add(new Transaction(0, playerName, "NOT_DEFINED", amount, 0));
            return er;
        } else {
            EconomyResponse er = new EconomyResponse(amount, pAm, EconomyResponse.ResponseType.FAILURE, "KO");
            return er;
        }
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer player, double amount) {
        return withdrawPlayer(player.getUniqueId().toString(), amount);
    }

    @Override
    public EconomyResponse withdrawPlayer(String playerName, String worldName, double amount) {
        return withdrawPlayer(playerName, amount);

    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer player, String worldName, double amount) {
        return withdrawPlayer(player.getUniqueId().toString(), amount);
    }

    @Override
    public EconomyResponse depositPlayer(String playerName, double amount) {
        Double pAm = getBalance(playerName);
        EconomyResponse er = new EconomyResponse(amount, pAm, EconomyResponse.ResponseType.SUCCESS, "OK");
        PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(UUID.fromString(playerName));
        pePlayer.getTransactions().add(new Transaction(0, "NOT_DEFINED", playerName, amount, 0));
        return er;
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer player, double amount) {
        return depositPlayer(player.getUniqueId().toString(), amount);
    }

    @Override
    public EconomyResponse depositPlayer(String playerName, String worldName, double amount) {
        return depositPlayer(playerName, amount);
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer player, String worldName, double amount) {
        return depositPlayer(player.getUniqueId().toString(), amount);
    }

    @Override
    public EconomyResponse createBank(String name, String player) {
        return null;
    }

    @Override
    public EconomyResponse createBank(String name, OfflinePlayer player) {
        return null;
    }

    @Override
    public EconomyResponse deleteBank(String name) {
        return null;
    }

    @Override
    public EconomyResponse bankBalance(String name) {
        return null;
    }

    @Override
    public EconomyResponse bankHas(String name, double amount) {
        return null;
    }

    @Override
    public EconomyResponse bankWithdraw(String name, double amount) {
        return null;
    }

    @Override
    public EconomyResponse bankDeposit(String name, double amount) {
        return null;
    }

    @Override
    public EconomyResponse isBankOwner(String name, String playerName) {
        return null;
    }

    @Override
    public EconomyResponse isBankOwner(String name, OfflinePlayer player) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String name, String playerName) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String name, OfflinePlayer player) {
        return null;
    }

    @Override
    public List<String> getBanks() {
        return null;
    }

    @Override
    public boolean createPlayerAccount(String playerName) {
        return true;
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer player) {
        return true;
    }

    @Override
    public boolean createPlayerAccount(String playerName, String worldName) {
        return true;
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer player, String worldName) {
        return true;
    }
}
