package org.dudullle.pixels.evolved.survival.alliances;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.database.AbstractDataClass;
import org.dudullle.pixels.evolved.survival.money.MoneyManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.dudullle.pixels.evolved.survival.utils.ReturnMessage.*;

/**
 * Classe représentant une alliance.
 */
public class PeAlliance extends AbstractDataClass {

    /**
     * Admins de l'alliance.
     */
    protected List<UUID> admins = new ArrayList<>();

    protected long saveDate = 0;
    /**
     * Membres de l'alliance.
     */
    protected List<UUID> members = new ArrayList<>();
    protected List<UUID> waitingApproval = new ArrayList<>();
    /**
     * Nom de l'alliance
     */
    private final String name;
    private AlliancePointOfInterest home;
    /**
     * Flags de l'alliance.
     */
    private final List<PeAllianceFlag> flags = new ArrayList<>();

    public PeAlliance(PePlayer creator, String name) {
        members.add(creator.getUuid());

        admins.add(creator.getUuid());

        this.name = name;
        AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.MEMBER, AllianceTransaction.DataOperation.ADD, creator.getUuid().toString());
        AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
        AllianceTransaction transaction2 = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.ADMIN, AllianceTransaction.DataOperation.ADD, creator.getUuid().toString());
        AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction2);
    }
    protected PeAlliance(String name, long saveDate) {
        this.name = name;
        this.saveDate = saveDate;
    }

    public AlliancePointOfInterest getHome() {
        return home;
    }

    public void setHome(AlliancePointOfInterest home) {
        this.home = home;
    }

    public ReturnMessage setHome(AlliancePointOfInterest home, PePlayer pePlayerAdmin) {
        if (!isAdmin(pePlayerAdmin)) {
            return ERROR_NOT_ADMIN;
        } else {
            this.home = home;
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.HOME, AllianceTransaction.DataOperation.SET, home.toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
            return SUCCESS;
        }
    }

    public List<UUID> getWaitingApproval() {
        return new ArrayList<>(waitingApproval);
    }

    public List<UUID> getMembers() {
        return new ArrayList<>(members);
    }

    public ReturnMessage approve(PePlayer playerToApprove, PePlayer pePlayerAdmin) {
        if (AllianceManager.INSTANCE.getAllianceByPePlayer(playerToApprove) != null) {
            return ERROR_ALREADY_IN_ALLIANCE;
        } else if (!waitingApproval.contains(playerToApprove.getUuid())) {
            return ERROR_NOT_WAITING;
        } else {
            ReturnMessage message = addMember(playerToApprove, pePlayerAdmin);
            if (message == SUCCESS) {
                waitingApproval.remove(playerToApprove.getUuid());
                AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.WAITING_MEMBER, AllianceTransaction.DataOperation.RMV, playerToApprove.getUuid().toString());
                AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
            }
            return message;
        }
    }

    public ReturnMessage disapprove(PePlayer playerToDisapprove, PePlayer pePlayerAdmin) {
        if (!isAdmin(pePlayerAdmin)) {
            return ERROR_NOT_ADMIN;
        } else {
            waitingApproval.remove(playerToDisapprove.getUuid());
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.WAITING_MEMBER, AllianceTransaction.DataOperation.RMV, playerToDisapprove.getUuid().toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
            return SUCCESS;
        }
    }

    public ReturnMessage addPlayerToApprovalList(PePlayer pePlayer) {
        if (isMember(pePlayer)) {
            return ERROR_ALREADY_MEMBER;
        } else if (waitingApproval.contains(pePlayer.getUuid())) {
            return ALREADY_WAITING;
        } else {
            waitingApproval.add(pePlayer.getUuid());
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.WAITING_MEMBER, AllianceTransaction.DataOperation.ADD, pePlayer.getUuid().toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
            return SUCCESS;
        }
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    public String getName() {
        return name;
    }

    /**
     * Check si le joueur est admin.
     *
     * @param pePlayer
     * @return BOOLEAN est admin ou pas
     */
    public Boolean isAdmin(PePlayer pePlayer) {
        return admins.contains(pePlayer.getUuid());
    }

    /**
     * Check si le joueur est membre.
     *
     * @param pePlayer
     * @return BOOLEAN est membre ou pas
     */
    public Boolean isMember(PePlayer pePlayer) {
        return members.contains(pePlayer.getUuid());
    }

    /**
     * Passer un membre à admin.
     *
     * @param pePlayerToPromote le joueur à promouvoir.
     * @param pePlayerAdmin     le joueur admin qui lance la commande.
     * @return
     */
    public ReturnMessage promote(PePlayer pePlayerToPromote, PePlayer pePlayerAdmin) {
        ReturnMessage message = genericChecksWithMemberAndAdmin(pePlayerToPromote, pePlayerAdmin);
        if (message == null) {
            if (isAdmin(pePlayerToPromote)) {
                message = ReturnMessage.ERROR_ALREADY_ADMIN;
            } else {
                admins.add(pePlayerToPromote.getUuid());
                AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.ADMIN, AllianceTransaction.DataOperation.ADD, pePlayerToPromote.getUuid().toString());
                AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
                message = ReturnMessage.SUCCESS;
            }
        }
        return message;
    }

    /**
     * Checks génériques pour savoir si un joueur est membre et un autre admin.
     *
     * @param pePlayer      Le joueur qui subit
     * @param pePlayerAdmin Le joueur admin qui agit
     * @return message de retour (null si succès, erreur sinon).
     */
    private ReturnMessage genericChecksWithMemberAndAdmin(PePlayer pePlayer, PePlayer pePlayerAdmin) {
        if (!isMember(pePlayer) || !isMember(pePlayerAdmin)) {
            return ReturnMessage.ERROR_NOT_IN_ALLIANCE;
        } else if (!isAdmin(pePlayerAdmin)) {
            return ReturnMessage.ERROR_NOT_ADMIN;
        } else {
            return null;
        }
    }

    /**
     * Ajoute un membre à l'alliance
     *
     * @param pePlayerToAdd Joueur à ajouter.
     * @param pePlayerAdmin Admin qui ajoute le joueur.
     * @return Message de retour (SUCCESS si ajout effectué avec succès, erreur sinon).
     */
    public ReturnMessage addMember(PePlayer pePlayerToAdd, PePlayer pePlayerAdmin) {
        if (!isAdmin(pePlayerAdmin)) {
            return ReturnMessage.ERROR_NOT_ADMIN;
        } else if (isMember(pePlayerToAdd)) {
            return ReturnMessage.ERROR_ALREADY_MEMBER;
        } else if (members.size() > 20) {
            return MAX_PLAYER;
        } else {
            members.add(pePlayerToAdd.getUuid());
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.MEMBER, AllianceTransaction.DataOperation.ADD, pePlayerToAdd.getUuid().toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
            return ReturnMessage.SUCCESS;
        }
    }

    /**
     * Passer un admin à membre.
     *
     * @param pePlayerToDemote le joueur à rétrograder.
     * @param pePlayerAdmin    le joueur admin qui lance la commande.
     * @return
     */
    public ReturnMessage demote(PePlayer pePlayerToDemote, PePlayer pePlayerAdmin) {
        ReturnMessage message = genericChecksWithMemberAndAdmin(pePlayerToDemote, pePlayerAdmin);
        if (message == null) {
            if (pePlayerToDemote == pePlayerAdmin) {
                message = ERROR_SELF_DESTRUCT;
            } else if (!isAdmin(pePlayerToDemote)) {
                message = ReturnMessage.ERROR_NOT_ADMIN;
            } else {
                admins.remove(pePlayerToDemote.getUuid());
                AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.ADMIN, AllianceTransaction.DataOperation.RMV, pePlayerToDemote.getUuid().toString());
                AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
                message = ReturnMessage.SUCCESS;
            }
        }
        return message;
    }

    /**
     * Retirer un membre.
     *
     * @param pePlayerToRemove le joueur à retirer.
     * @param pePlayerAdmin    le joueur admin qui lance la commande.
     * @return Message de retour (SUCCESS si réussite).
     */
    public ReturnMessage removePlayer(PePlayer pePlayerToRemove, PePlayer pePlayerAdmin) {
        ReturnMessage message = genericChecksWithMemberAndAdmin(pePlayerToRemove, pePlayerAdmin);
        if (message == null) {
            admins.remove(pePlayerToRemove.getUuid());
            members.remove(pePlayerToRemove.getUuid());
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.ADMIN, AllianceTransaction.DataOperation.RMV, pePlayerToRemove.getUuid().toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
            AllianceTransaction transaction2 = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.MEMBER, AllianceTransaction.DataOperation.RMV, pePlayerToRemove.getUuid().toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction2);
            message = ReturnMessage.SUCCESS;
        }
        return message;
    }

    /**
     * Méthode qui retourne si l'on est dans un flag de l'alliance.
     *
     * @param chunkX coordonées X du chunk.
     * @param chunkZ coordonées Z du chunk.
     * @return Flag si existe, null sinon.
     */
    public PeAllianceFlag getFlagAt(int chunkX, int chunkZ) {
        Optional<PeAllianceFlag> optionalPeAllianceFlag = flags.stream()
                .filter(x -> x.getxLocation() == chunkX && x.getzLocation() == chunkZ).findFirst();
        return optionalPeAllianceFlag.orElse(null);
    }

    public ReturnMessage addFlag(PeAllianceFlag flag) {
        return this.addFlag(flag, false);
    }

    /**
     * Méthode pour ajouter un flag.
     *
     * @param flag Le flag.
     * @return Message de retour (succès).
     */
    public ReturnMessage addFlag(PeAllianceFlag flag, boolean fromDatabase) {
        flags.add(flag);
        if (!fromDatabase) {
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.FLAG, AllianceTransaction.DataOperation.ADD, flag.toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
        }
        return ReturnMessage.SUCCESS;
    }

    /**
     * Permet de retirer un flag
     *
     * @param chunkX
     * @param chunkZ
     * @return
     */
    public ReturnMessage removeFlag(int chunkX, int chunkZ, PePlayer pePlayerAdmin) {
        if (!isAdmin(pePlayerAdmin)) {
            return ReturnMessage.ERROR_NOT_ADMIN;
        } else {
            PeAllianceFlag flag = getFlagAt(chunkX, chunkZ);
            if (flag == null) {
                return ReturnMessage.FLAG_NOT_FOUND;
            }
            EconomyResponse economyResponse = MoneyManager.INSTANCE.withdrawPlayer(pePlayerAdmin.getUuid().toString(),
                    flag.getRemovalCosts());
            if (economyResponse.type == EconomyResponse.ResponseType.FAILURE) {
                return ReturnMessage.NO_ENOUGH_MONEY;
            } else {
                flags.remove(flag);
                AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.FLAG, AllianceTransaction.DataOperation.RMV, flag.toString());
                AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
                //Suppression de la région
                AllianceManager.INSTANCE.removeWorldguardRegion(this, flag.getxLocation(), flag.getzLocation(), flag, Bukkit.getWorld(flag.getWorldName()));
                return ReturnMessage.SUCCESS;
            }
        }
    }

    public ReturnMessage removeFlag(PeAllianceFlag flag, Boolean fromDatabase) {
        flags.remove(flag);
        if (!fromDatabase) {
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.FLAG, AllianceTransaction.DataOperation.RMV, flag.toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
        }
        //Suppression de la région :
        AllianceManager.INSTANCE.removeWorldguardRegion(this, flag.getxLocation(), flag.getzLocation(), flag, Bukkit.getWorld(flag.getWorldName()));
        return SUCCESS;
    }

    public ReturnMessage destroyFlag(int chunkX, int chunkZ, PeAlliance ennemyAlliance) {
        PeAllianceFlag flag = getFlagAt(chunkX, chunkZ);
        if (flag == null) {
            return ReturnMessage.FLAG_NOT_FOUND;
        } else if (ennemyAlliance == this) {
            return ReturnMessage.SELF_DESTRUCTION_NOT_AVAILABLE;
        } else {
            flags.remove(flag);
            //TODO ajout du contenu du flag à l'alliance ennemie
            AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.FLAG, AllianceTransaction.DataOperation.RMV, flag.toString());
            AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
            return ReturnMessage.FLAG_DESTROY_SUCCESS;
        }
    }

    public List<Player> getOnlinePlayers() {
        List<Player> players = new ArrayList<>();
        for (UUID uuid : members) {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
            if (offlinePlayer.isOnline()) {
                players.add((Player) offlinePlayer);
            }
        }
        return players;
    }

    public Boolean touchesFlag(int chunkX, int chunkZ) {
        if (!flags.isEmpty()) {
            for (PeAllianceFlag masterFlag : flags) {
                //Calcul de l'écart de coordonées.
                int dChunkX = masterFlag.getxLocation() - chunkX;
                int dChunkZ = masterFlag.getzLocation() - chunkZ;
                if ((dChunkX == 1 || dChunkX == -1) && dChunkZ == 0) {
                    return true;
                } else if ((dChunkZ == 1 || dChunkZ == -1) && dChunkX == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<PeAllianceFlag> getFlags() {
        return new ArrayList<>(flags);
    }

    public List<UUID> getAdmins() {
        return new ArrayList<>(members);
    }

    public void updateClaims() {
        if (!((PluginMain) PluginMain.getInstance()).isLobby()) {
            flags.forEach(x -> AllianceManager.INSTANCE.updatePlayersRegion(this, x.getxLocation(), x.getzLocation(), x, Bukkit.getWorld(x.getWorldName())));
        }
    }

    public ReturnMessage deleteAlliance(PePlayer admin, Boolean force) {
        if (!force && !isAdmin(admin)) {
            return ERROR_NOT_ADMIN;
        } else if (!force && members.size() > 1) {
            admin.sendMessage("Il y a encore des membres dans l'alliance. Vous devez les expulser avant d'effectuer cela.");
            return ERROR_ALREADY_IN_ALLIANCE;
        } else {
            if (!force) {
                admin.sendMessage("§cVotre alliance va être supprimée.");
                admin.sendMessage("§4Si vous ne vouliez pas effectuer cette action, vous avez 1h pour contacter le staff et ne pas recréer/rejoindre d'alliance");
            }
            List<PeAllianceFlag> flagsCopy = new ArrayList<>(flags);
            for (PeAllianceFlag flag : flagsCopy) {
                removeFlag(flag, force);
            }
            if (!force) {
                AllianceTransaction transaction = new AllianceTransaction(this.getName(), 0, AllianceTransaction.DataType.ALL, AllianceTransaction.DataOperation.RMV, "Suppression par " + admin.getName());
                AllianceDatabaseManager.INSTANCE.addAllianceTransactionToSaveQueue(transaction);
                AllianceManager.INSTANCE.removeAlliance(admin);
            } else {
                AllianceManager.INSTANCE.removeAlliance(this);
            }
            return SUCCESS;
        }
    }
}
