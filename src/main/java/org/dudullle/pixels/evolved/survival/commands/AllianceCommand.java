package org.dudullle.pixels.evolved.survival.commands;

import com.destroystokyo.paper.Title;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.dudullle.pixels.evolved.survival.alliances.AllianceManager;
import org.dudullle.pixels.evolved.survival.alliances.PeAlliance;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlag;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlagObject;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.listeners.InventoryListener;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.utils.ItemStackUtils;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;

import java.util.*;

public class AllianceCommand extends AbstractCommand {
    public static final AllianceCommand INSTANCE = new AllianceCommand();
    public static final String COMMAND_NAME = "alliance";

    public static final String PREFIX = "ALLIANCE";
    public static final String NO_ALLIANCE_MAIN_MENU = " Choisir !";
    public static final String NO_ALLIANCE_MAIN_MENU_JOIN = " Rejoindre !";
    public static final String ALLIANCE_MAIN_MENU = " Menu principal";
    public static final String ALLIANCE_APPROVE = " Accepter / Refuser";
    public static final String ALLIANCE_KICK_RANK = " Promo / Kick";
    public static final String ITEM_TITLE_COLOR = "§5§l";
    public static final String ITEM_DESCRIPTION_NORMAL = "§b";
    public static final String ITEM_DESCRIPTION_LOW = "§7";
    public static final String SCOREBOARD_SETTINGS = " Scoreboard";

    public AllianceCommand() {
        super(COMMAND_NAME);
        Map<String, String> subCommandDescription = new TreeMap<>();
        subCommandDescription.put("help", "Effectuez cette commande pour ouvrir l'inventaire de gestion de votre alliance.");
        this.setSubCommandDescription(subCommandDescription);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
            if (pePlayer != null) {
                PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
                if (alliance == null) {
                    p.openInventory(createNoAllianceInventory());
                } else {
                    if (args.length != 0) {
                        String confirmationMessage = String.join(" ", args);
                        if ("je veux supprimer mon alliance".equalsIgnoreCase(confirmationMessage)) {
                            alliance.deleteAlliance(pePlayer, false);
                        }
                    } else {
                        p.openInventory(createAllianceInventory(alliance, p));
                    }
                }
            } else {
                p.sendMessage("§4Une erreur est survenue, merci de vous déconnecter et vous reconnecter.");
                p.sendMessage("§cSi le problème perciste, contactez un administrateur ! (IKWOR de préférence)");
            }
        } else {
            sender.sendMessage("La commande n'est pas encore prête pour la console");
        }
        return true;
    }

    public InventoryListener.CallbackType handleInventoryClick(InventoryClickEvent event, PePlayer pePlayer) {
        InventoryView inv = event.getView();
        if (inv.getTitle().equalsIgnoreCase(PREFIX + NO_ALLIANCE_MAIN_MENU)) {
            handleInventoryClickNoAllianceMainMenu(event, pePlayer);
        } else if (inv.getTitle().equalsIgnoreCase(PREFIX + NO_ALLIANCE_MAIN_MENU_JOIN)) {
            handleInventoryClickNoAllianceMainMenuJoin(event, pePlayer, inv);
        } else if (inv.getTitle().equalsIgnoreCase(PREFIX + ALLIANCE_MAIN_MENU)) {
            handleInventoryClickAllianceMainMenu(event, pePlayer, inv);
        } else if (inv.getTitle().equalsIgnoreCase(PREFIX + ALLIANCE_APPROVE)) {
            handleInventoryClickAllianceApproval(event, pePlayer, inv);
        } else if (inv.getTitle().equalsIgnoreCase(PREFIX + ALLIANCE_KICK_RANK)) {
            handleInventoryClickMemberManage(event, pePlayer, inv);
        } else if (inv.getTitle().equalsIgnoreCase(PREFIX + SCOREBOARD_SETTINGS)) {
            handleScoreBoardParamInventory(event, pePlayer, inv);
        }
        return InventoryListener.CallbackType.BEEP_ERROR;
    }

    public Inventory createNoAllianceInventory() {
        Inventory inv = Bukkit.createInventory(null, 9, PREFIX + NO_ALLIANCE_MAIN_MENU);
        ItemStack createAlliance = ItemStackUtils.get(ITEM_TITLE_COLOR + "Créer une alliance",
                Material.WRITTEN_BOOK, ITEM_DESCRIPTION_NORMAL + "Cliquez ici pour créer",
                ITEM_DESCRIPTION_NORMAL + "votre alliance.");

        ItemStack joinAlliance = ItemStackUtils.get(ITEM_TITLE_COLOR + "Rejoindre une alliance",
                Material.BIRCH_DOOR, ITEM_DESCRIPTION_NORMAL + "Cliquez ici pour rejoindre",
                ITEM_DESCRIPTION_NORMAL + "une alliance.", ITEM_DESCRIPTION_LOW + "vous devrez attendre l'approbation.");

        inv.setItem(3, createAlliance);
        inv.setItem(6, joinAlliance);
        return inv;
    }

    public Inventory createNoAllianceJoinListInventory(int page) {
        Inventory inv = Bukkit.createInventory(null, 9 * 4, PREFIX + NO_ALLIANCE_MAIN_MENU_JOIN);
        //Système de pages
        ItemStack pageNumber = ItemStackUtils.get(ITEM_TITLE_COLOR + "Page actuelle", Material.PAPER);
        pageNumber.setAmount(page);

        ItemStack pagePlus = ItemStackUtils.get(ITEM_TITLE_COLOR + "Page suivante", Material.GREEN_WOOL);
        ItemStack pageLess = ItemStackUtils.get(ITEM_TITLE_COLOR + "Page précédente", Material.ORANGE_WOOL);
        List<PeAlliance> allianceList = AllianceManager.INSTANCE.getAlliances();
        for (int i = (page - 1) * 9; i < (page * 9 * 3); i++) {
            if (i < allianceList.size()) {
                PeAlliance alliance = allianceList.get(i);
                inv.addItem(ItemStackUtils.get(alliance.getName(), Material.ENDER_EYE
                        , ITEM_DESCRIPTION_LOW + "Cliquez pour rejoindre"));
            }
        }
        inv.setItem(9 * 4 - 3, pageNumber);
        if (page != 1) {
            inv.setItem(9 * 4 - 2, pageLess);
        }
        if (allianceList.size() > page * 9 * 3) {
            inv.setItem(9 * 4 - 1, pagePlus);
        }
        return inv;
    }

    public InventoryListener.CallbackType handleInventoryClickNoAllianceMainMenu(InventoryClickEvent event, PePlayer pePlayer) {
        Player p = (Player) event.getWhoClicked();
        if (event.getSlot() == 3) {
            ReturnMessage message = AllianceManager.INSTANCE.createAlliance(pePlayer);

            p.closeInventory();
            p.sendMessage("§7Code retour : " + message);

            switch (message) {
                case SUCCESS:
                    p.sendTitle(new Title("§aAlliance créée avec succès",
                            "§bFaites /alliance pour afficher les options :D", 10, 100, 10));

                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.MASTER));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.REINFORCED));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.REINFORCED));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.REINFORCED));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.REINFORCED));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.AGRICULTURE));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.MINING));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.NORMAL));
                    p.getInventory().addItem(new PeAllianceFlagObject(PeAllianceFlag.FlagType.NORMAL));

                    p.sendMessage("§a---------------------------------");
                    p.sendMessage("§dITEMS RARES RECUS !");
                    p.sendMessage("§dx1 Item de claim principal, permet de démarrer une nouvelle zone");
                    p.sendMessage("§dx8 Item de claim secondaire, permet d'agrandir une zone");
                    p.sendMessage("§a---------------------------------");

                    return InventoryListener.CallbackType.BEEP_NORMAL;
                case ERROR_ALREADY_MEMBER:
                    p.sendTitle(new Title("§cOh non ! Une erreur !",
                            "§4Il semble que vous soyez déjà dans une faction", 10, 100, 10));
                    return InventoryListener.CallbackType.BEEP_ERROR;
                default:
                    p.sendTitle(new Title("§cOh non ! Une erreur " + message,
                            "§4Cette erreur n'est pas prise en charge. Contactez le staff",
                            10, 100, 10));
                    return InventoryListener.CallbackType.BEEP_ERROR;
            }
        } else if (event.getSlot() == 6) {
            //Ouverture de la liste des factions
            p.openInventory(createNoAllianceJoinListInventory(1));
            return InventoryListener.CallbackType.BEEP_NORMAL;
        } else {
            return InventoryListener.CallbackType.BEEP_ERROR;
        }
    }

    public InventoryListener.CallbackType handleInventoryClickNoAllianceMainMenuJoin(InventoryClickEvent event, PePlayer pePlayer, InventoryView inv) {
        Player p = (Player) event.getWhoClicked();
        ItemStack itemStack = event.getInventory().getItem(event.getSlot());
        if (itemStack == null) {
            return InventoryListener.CallbackType.BEEP_ERROR;
        } else {
            ItemStack pageNumberItem = event.getInventory().getItem(9 * 4 - 3);
            if (event.getSlot() == 9 * 4 - 2) {
                //Page précédente
                p.openInventory(createNoAllianceJoinListInventory(pageNumberItem.getAmount() - 1));
                return InventoryListener.CallbackType.BEEP_NORMAL;
            } else if (event.getSlot() == 9 * 4 - 1) {
                //page suivante
                p.openInventory(createNoAllianceJoinListInventory(pageNumberItem.getAmount() + 1));
                return InventoryListener.CallbackType.BEEP_NORMAL;
            } else {
                //On a cliqué sur une faction
                String allianceName = itemStack.getItemMeta().getDisplayName();
                PeAlliance alliance = AllianceManager.INSTANCE.getByName(allianceName);
                if (alliance == null) {
                    return InventoryListener.CallbackType.BEEP_ERROR;
                } else {
                    ReturnMessage message = alliance.addPlayerToApprovalList(pePlayer);
                    p.closeInventory();
                    p.sendMessage("§7Code retour : " + message);
                    switch (message) {
                        case SUCCESS:
                            p.sendTitle(new Title("§aEn attente",
                                    "§bVous êtes en attente pour rejoindre cette faction",
                                    10, 100, 10));
                            return InventoryListener.CallbackType.BEEP_NORMAL;
                        case ALREADY_WAITING:
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Il semble que vous soyez déjà en attente pour cette faction",
                                    10, 100, 10));
                            return InventoryListener.CallbackType.BEEP_ERROR;
                        case ERROR_ALREADY_MEMBER:
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Il semble que vous soyez déjà dans cette faction",
                                    10, 100, 10));
                            return InventoryListener.CallbackType.BEEP_ERROR;
                        default:
                            p.sendTitle(new Title("§cOh non ! Une erreur " + message,
                                    "§4Cette erreur n'est pas prise en charge. Contactez le staff",
                                    10, 100, 10));
                            return InventoryListener.CallbackType.BEEP_ERROR;
                    }
                }
            }
        }
    }

    public Inventory createAllianceInventory(PeAlliance alliance, Player p) {
        Inventory inv = Bukkit.createInventory(null, 9, PREFIX + ALLIANCE_MAIN_MENU);
        ItemStack homeAlliance = ItemStackUtils.get(ITEM_TITLE_COLOR + "Home d'alliance",
                Material.DARK_OAK_DOOR, ITEM_DESCRIPTION_NORMAL + "Cliquez pour se téléporter",
                ITEM_DESCRIPTION_NORMAL + "sur le territoire de votre alliance.", ITEM_DESCRIPTION_LOW + "Pour en set un, ouvrez ce menu",
                ITEM_DESCRIPTION_LOW + "lorsque vous êtes sur le territoire d'alliance.");

        ItemStack setHome = ItemStackUtils.get(ITEM_TITLE_COLOR + "Mettre le Home d'alliance",
                Material.BEACON, ITEM_DESCRIPTION_NORMAL + "Cliquez pour définir votre hOOOOOme ici",
                ITEM_DESCRIPTION_NORMAL + "sur le territoire de votre alliance (REQUIS).");

        ItemStack approve = ItemStackUtils.get(ITEM_TITLE_COLOR + "Accepter/Refuser un joueur",
                Material.CHEST, ITEM_DESCRIPTION_NORMAL + "Cliquez pour accepter les joueurs en attente",
                ITEM_DESCRIPTION_NORMAL + "... ou les refuser");

        ItemStack members = ItemStackUtils.get(ITEM_TITLE_COLOR + "Rank/Unrank/kick un membre",
                Material.ACACIA_BOAT, ITEM_DESCRIPTION_NORMAL + "Cliquez pour gérer les membres",
                ITEM_DESCRIPTION_NORMAL + "... et même les expulser");

        ItemStack scoreboard = ItemStackUtils.get(ITEM_TITLE_COLOR + "Affichage du scoreboard",
                Material.SPRUCE_SIGN, ITEM_DESCRIPTION_NORMAL + "Cliquez pour gérer votre interface",
                ITEM_DESCRIPTION_LOW + "Afficher et masquer des champs");

        ItemStack alerts = ItemStackUtils.get(ITEM_TITLE_COLOR + "Gestion des alertes",
                Material.TNT, ITEM_DESCRIPTION_NORMAL + "Attaques, entrées sur territoire, ...",
                ITEM_DESCRIPTION_LOW + "Attention ça coute des PX par jour !", "§cBientôt ...");

        ItemStack details = ItemStackUtils.get(ITEM_TITLE_COLOR + "Détails sur l'alliance",
                Material.DIAMOND, ITEM_DESCRIPTION_NORMAL + "Statistiques & Classements",
                ITEM_DESCRIPTION_LOW + "Apprenez où vous améliorer !", "§cBientôt ...");

        ItemStack delAlliance = ItemStackUtils.get(ITEM_TITLE_COLOR + "Supprimer",
                Material.BARRIER, ITEM_DESCRIPTION_NORMAL + "Supprimer l'alliance",
                ITEM_DESCRIPTION_LOW + "DANGER");

        inv.setItem(1, homeAlliance);
        if (alliance.getFlagAt(p.getLocation().getChunk().getX(), p.getLocation().getChunk().getZ()) != null) {
            inv.setItem(2, setHome);
        }
        inv.setItem(3, approve);
        inv.setItem(4, members);
        inv.setItem(5, scoreboard);
        inv.setItem(6, alerts);
        inv.setItem(7, details);
        inv.setItem(8, delAlliance);

        return inv;
    }

    public InventoryListener.CallbackType handleInventoryClickAllianceMainMenu(InventoryClickEvent event, PePlayer pePlayer, InventoryView inv) {
        InventoryListener.CallbackType callbackType = InventoryListener.CallbackType.BEEP_NORMAL;
        Player p = (Player) event.getWhoClicked();
        PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
        if (event.getInventory().getItem(event.getSlot()) != null) {
            switch (event.getSlot()) {
                case 1:
                    AlliancePointOfInterest home = alliance.getHome();
                    if (home != null) {
                        Location l = home.toLocation();
                        if (l != null) {
                            p.teleport(l);
                            p.playSound(l, Sound.ENTITY_FOX_TELEPORT, 100, 0);
                        } else {
                            p.sendTitle(new Title("§eRejoignez le Royaume avant de faire ceci", "", 10, 100, 10));
                        }
                    } else {
                        p.sendTitle(new Title("§cL'alliance n'a pas de home", "§cUtilise ton §e/home §cà la place", 10, 100, 10));
                    }
                    //Téléportation au home
                    break;
                case 2:
                    if (alliance.setHome(new AlliancePointOfInterest(p.getLocation()), pePlayer) == ReturnMessage.SUCCESS) {
                        p.sendTitle(new Title("§aHome de faction modifié", "§aVous pouvez aussi utiliser votre /home personnel !", 10, 100, 10));
                        p.closeInventory();
                    } else {
                        p.closeInventory();
                        p.sendTitle(new Title("§cVous n'êtes pas admin :(", "§cUtilise ton §e/home §cà la place", 10, 100, 10));
                        callbackType = InventoryListener.CallbackType.BEEP_ERROR;
                    }
                    break;
                case 3:
                    //Traitement aprobation des joueurs (ouverture menu)
                    p.openInventory(createApproveInventory(alliance, pePlayer));
                    break;
                case 4:
                    //Traitement gestion des membres (ouverture menu)
                    p.openInventory(createMembersInventory(alliance, pePlayer));
                    break;
                case 5:
                    p.openInventory(createScoreBoardParamInventory(alliance, pePlayer));
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    p.sendMessage("§4Attention danger ! Cette action va supprimer l'alliance.");
                    p.sendMessage("§cLes claims seront supprimés ainsi que le home d'alliance.");
                    p.sendMessage("§cVous devrez rejoindre ou recréer une alliance pour jouer.");
                    p.sendMessage("§d---------------------------------------------------------");
                    p.sendMessage("§cFaites §c§l/alliance je veux supprimer mon alliance §cpour continuer.");
                    p.closeInventory();
                    break;
            }
        }
        return callbackType;
    }

    public Inventory createApproveInventory(PeAlliance alliance, PePlayer pePlayer) {
        Inventory inv = Bukkit.createInventory(null, 9 * 6, PREFIX + ALLIANCE_APPROVE);
        List<UUID> waitingPlayer = alliance.getWaitingApproval();
        for (int i = 0; i < Integer.min(9 * 6, waitingPlayer.size()); i++) {
            OfflinePlayer player = Bukkit.getOfflinePlayer(waitingPlayer.get(i));

            ItemStack playerItem = ItemStackUtils.getHead(player, ITEM_DESCRIPTION_NORMAL + "CLIC GAUCHE : §aaccepter",
                    ITEM_DESCRIPTION_NORMAL + "CLIC DROIT : §crefuser");
            inv.addItem(playerItem);
        }
        return inv;
    }

    public InventoryListener.CallbackType handleInventoryClickAllianceApproval(InventoryClickEvent event, PePlayer pePlayer, InventoryView inv) {
        Player p = (Player) event.getWhoClicked();
        InventoryListener.CallbackType beep = InventoryListener.CallbackType.BEEP_ERROR;
        PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
        ItemStack item = event.getInventory().getItem(event.getSlot());
        if (item != null) {
            SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
            OfflinePlayer offlinePlayer = skullMeta.getOwningPlayer();
            if (offlinePlayer != null) {
                PePlayer pePlayerToActOn = PlayersManager.INSTANCE.getPePlayer(offlinePlayer.getUniqueId());
                if (pePlayerToActOn != null) {
                    ReturnMessage message = ReturnMessage.FAIL;
                    if (event.getClick().equals(ClickType.RIGHT)) {
                        message = alliance.disapprove(pePlayerToActOn, pePlayer);
                    } else {
                        message = alliance.approve(pePlayerToActOn, pePlayer);
                    }
                    p.sendMessage("§7Code retour : " + message);
                    p.openInventory(createApproveInventory(alliance, pePlayer));
                    alliance.updateClaims();
                    switch (message) {
                        case SUCCESS:
                            p.sendMessage("§aAction sur " + pePlayerToActOn.getName() + " effectuée.");
                            break;
                        case ERROR_NOT_ADMIN:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Petit malin :P tu dois être admin de ta faction",
                                    10, 100, 10));
                            break;
                        case ERROR_ALREADY_IN_ALLIANCE:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Le joueur a déjà rejoint une alliance",
                                    10, 100, 10));
                            break;
                        case ERROR_NOT_WAITING:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Le joueur n'est plus dans la liste",
                                    10, 100, 10));
                            break;
                        case ERROR_ALREADY_MEMBER:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Le joueur est déjà dans l'alliance",
                                    10, 100, 10));
                            break;
                    }
                }
            }
        }
        return beep;
    }

    public Inventory createMembersInventory(PeAlliance alliance, PePlayer pePlayer) {
        Inventory inv = Bukkit.createInventory(null, 9 * 6, PREFIX + ALLIANCE_KICK_RANK);
        List<UUID> members = alliance.getMembers();
        for (int i = 0; i < Integer.min(20, members.size()); i++) {
            OfflinePlayer player = Bukkit.getOfflinePlayer(members.get(i));
            PePlayer pePlayerToDraw = PlayersManager.INSTANCE.getPePlayer(player.getUniqueId());
            ArrayList<String> lore = null;
            if (alliance.isAdmin(pePlayerToDraw)) {
                lore = new ArrayList<>();
                lore.add(ITEM_DESCRIPTION_NORMAL + "CLIC DROIT : §4dégrader");
            } else {
                lore = new ArrayList<>();
                lore.add(ITEM_DESCRIPTION_NORMAL + "CLIC DROIT : §4§lExpulser");
                lore.add(ITEM_DESCRIPTION_NORMAL + "CLIC GAUCHE : §a§lPromotion !");
            }
            ItemStack playerItem = ItemStackUtils.getHead(player, lore);
            if (alliance.isAdmin(pePlayerToDraw)) {
                playerItem.addEnchantment(Enchantment.BINDING_CURSE, 1);
            }
            inv.addItem(playerItem);
        }
        return inv;
    }

    public InventoryListener.CallbackType handleInventoryClickMemberManage(InventoryClickEvent event, PePlayer pePlayer, InventoryView inv) {
        Player p = (Player) event.getWhoClicked();
        InventoryListener.CallbackType beep = InventoryListener.CallbackType.BEEP_ERROR;
        PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
        ItemStack item = event.getInventory().getItem(event.getSlot());
        if (item != null) {
            SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
            OfflinePlayer offlinePlayer = skullMeta.getOwningPlayer();
            if (offlinePlayer != null) {
                PePlayer pePlayerToActOn = PlayersManager.INSTANCE.getPePlayer(offlinePlayer.getUniqueId());
                if (pePlayerToActOn != null) {
                    ReturnMessage message = ReturnMessage.FAIL;
                    if (event.getClick().equals(ClickType.RIGHT)) {
                        if (alliance.isAdmin(pePlayerToActOn)) {
                            message = alliance.demote(pePlayerToActOn, pePlayer);
                        } else {
                            message = alliance.removePlayer(pePlayerToActOn, pePlayer);
                        }
                    } else {
                        message = alliance.promote(pePlayerToActOn, pePlayer);
                    }
                    p.sendMessage("§7Code retour : " + message);
                    p.openInventory(createMembersInventory(alliance, pePlayer));
                    alliance.updateClaims();
                    switch (message) {
                        case SUCCESS:
                            p.sendMessage("§aAction sur " + pePlayerToActOn.getName() + " effectuée.");
                            break;
                        case ERROR_NOT_ADMIN:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Petit malin :P tu dois être admin de ta faction",
                                    10, 100, 10));
                            break;
                        case ERROR_ALREADY_IN_ALLIANCE:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Le joueur a déjà rejoint une alliance",
                                    10, 100, 10));
                            break;
                        case ERROR_NOT_WAITING:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Le joueur n'est plus dans la liste",
                                    10, 100, 10));
                            break;
                        case ERROR_ALREADY_MEMBER:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Le joueur est déjà dans l'alliance",
                                    10, 100, 10));
                            break;
                        case ERROR_SELF_DESTRUCT:
                            p.closeInventory();
                            p.sendTitle(new Title("§cOh non ! Une erreur !",
                                    "§4Tu ne peux pas te virer tout seul :P",
                                    10, 100, 10));
                            break;
                    }
                }
            }
        }
        return beep;
    }

    public Inventory createScoreBoardParamInventory(PeAlliance alliance, PePlayer pePlayer) {
        Inventory inv = Bukkit.createInventory(null, 9, PREFIX + SCOREBOARD_SETTINGS);
        inv.setItem(1, ItemStackUtils.get("Partie alliance", pePlayer.isSbShowAlliance() ? Material.GREEN_WOOL : Material.RED_WOOL, "Cliquez et regardez !"));
        inv.setItem(3, ItemStackUtils.get("Partie Events", pePlayer.isSbShowEvent() ? Material.GREEN_WOOL : Material.RED_WOOL, "Cliquez et regardez !"));
        inv.setItem(5, ItemStackUtils.get("Partie Heure et lag", pePlayer.isSbShowHourAndLag() ? Material.GREEN_WOOL : Material.RED_WOOL, "Cliquez et regardez !"));
        inv.setItem(7, ItemStackUtils.get("Partie Emplacement", pePlayer.isSbShowLocationInformation() ? Material.GREEN_WOOL : Material.RED_WOOL, "Cliquez et regardez !"));
        return inv;
    }

    public InventoryListener.CallbackType handleScoreBoardParamInventory(InventoryClickEvent event, PePlayer pePlayer, InventoryView inv) {
        switch (event.getSlot()) {
            case 1:
                pePlayer.setSbShowAlliance(!pePlayer.isSbShowAlliance());
                break;
            case 3:
                pePlayer.setSbShowEvent(!pePlayer.isSbShowEvent());
                break;
            case 5:
                pePlayer.setSbShowHourAndLag(!pePlayer.isSbShowHourAndLag());
                break;
            case 7:
                pePlayer.setSbShowLocationInformation(!pePlayer.isSbShowLocationInformation());
                break;
        }
        inv.getPlayer().openInventory(createScoreBoardParamInventory(null, pePlayer));
        return InventoryListener.CallbackType.BEEP_NORMAL;
    }

}
