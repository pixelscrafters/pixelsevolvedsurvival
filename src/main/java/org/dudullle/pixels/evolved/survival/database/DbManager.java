package org.dudullle.pixels.evolved.survival.database;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DbManager {
    public static final DbManager INSTANCE = new DbManager();

    private final MysqlDataSource dataSource;

    protected DbManager() {
        dataSource = new MysqlDataSource();
        dataSource.setUser("dModeratorManager");
        dataSource.setPassword("vidvin");
        dataSource.setServerName("localhost");
        dataSource.setDatabaseName("SURV_PIXELS_EVOLVED");
    }

    public Connection openConnection() throws SQLException {
        Connection conn = dataSource.getConnection();
        return conn;
    }

    public void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }

}
