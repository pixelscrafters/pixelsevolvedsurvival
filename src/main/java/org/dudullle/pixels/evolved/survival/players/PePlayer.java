package org.dudullle.pixels.evolved.survival.players;

import com.google.gson.annotations.Expose;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.alliances.AllianceManager;
import org.dudullle.pixels.evolved.survival.alliances.PeAlliance;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlag;
import org.dudullle.pixels.evolved.survival.database.AbstractDataClass;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.money.Transaction;
import org.dudullle.pixels.evolved.survival.scoreboard.PeScoreboard;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.utils.Lag;
import org.dudullle.pixels.evolved.survival.utils.StringUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class PePlayer extends AbstractDataClass {
    private UUID uuid;
    private long id;
    private long lastLogin;
    private String name;

    @Expose(serialize = false, deserialize = false)
    private PeScoreboard peScoreboard;

    private AlliancePointOfInterest home;
    private Boolean isVanished;

    @Expose(serialize = false, deserialize = false)
    private UUID uuidWaitingForTp;
    private Boolean isMute;
    @Expose(serialize = false, deserialize = false)
    private List<PePlayerData> pePlayerData = new ArrayList<>();
    /**
     * ONLY GET
     */
    private Boolean isBanned;
    /**
     * ONLY GET
     */
    private long banTime;
    @Expose(serialize = false, deserialize = false)
    private List<Transaction> transactions;
    private boolean sbShowHourAndLag = true;
    private boolean sbShowLocationInformation = true;
    private boolean sbShowEvent = true;
    private boolean sbShowAlliance = true;
    private final boolean sbInitialized = false;
    private boolean isUsingClaimItem = false;
    public PePlayer() {
        id = -1;
        hasBeenCreated = true;
    }

    public void readPePlayerData(PePlayerData playerData) {
        pePlayerData.add(playerData);
        switch (playerData.dataType) {
            case HOME:
                AlliancePointOfInterest home = AlliancePointOfInterest.fromJson(playerData.data);
                this.home = home;
                sendSystemNotification("Home synchronisé depuis la base.");
                break;
            case BAN:
                break;
            case BAN_TIME:
                break;
            case KICK:
                break;
        }
    }

    public void sendMessage(String message) {
        Player p = Bukkit.getPlayer(uuid);
        if (p != null && p.isOnline()) {
            p.sendMessage(message);
        }
    }

    public void sendSystemNotification(String notification) {
        this.sendMessage("§7[SURVIE] " + notification);
    }

    public List<PePlayerData> getPePlayerData() {
        return new ArrayList<>(pePlayerData);
    }

    public AlliancePointOfInterest getHome() {
        return home;
    }

    public void setHome(AlliancePointOfInterest home) {
        PePlayerData data = new PePlayerData(uuid, PePlayerDataType.HOME, home.toString(), System.currentTimeMillis());
        pePlayerData.add(data);
        this.home = home;
    }

    public Boolean getVanished() {
        return isVanished;
    }

    public PePlayer getUuidWaitingForTp() {
        return PlayersManager.INSTANCE.getPePlayer(uuidWaitingForTp);
    }

    public void setUuidWaitingForTp(UUID uuidWaitingForTp) {
        this.uuidWaitingForTp = uuidWaitingForTp;
    }

    public UUID getUuidWaitingForTpUuid() {
        return uuidWaitingForTp;
    }

    public Boolean getMute() {
        return isMute;
    }

    public boolean isSbShowHourAndLag() {
        return sbShowHourAndLag;
    }

    public void setSbShowHourAndLag(boolean sbShowHourAndLag) {
        this.sbShowHourAndLag = sbShowHourAndLag;
    }

    public boolean isSbShowLocationInformation() {
        return sbShowLocationInformation;
    }

    public void setSbShowLocationInformation(boolean sbShowLocationInformation) {
        this.sbShowLocationInformation = sbShowLocationInformation;
    }

    public boolean isSbShowEvent() {
        return sbShowEvent;
    }

    public void setSbShowEvent(boolean sbShowEvent) {
        this.sbShowEvent = sbShowEvent;
    }

    public boolean isSbShowAlliance() {
        return sbShowAlliance;
    }

    public void setSbShowAlliance(boolean sbShowAlliance) {
        this.sbShowAlliance = sbShowAlliance;
    }

    public boolean isUsingClaimItem() {
        return isUsingClaimItem;
    }

    public void setUsingClaimItem(boolean usingClaimItem) {
        isUsingClaimItem = usingClaimItem;
    }

    public Boolean getBanned() {
        return isBanned;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
        hasBeenModified = true;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(long lastLogin) {
        this.lastLogin = lastLogin;
        hasBeenModified = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        hasBeenModified = true;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
        hasBeenModified = true;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Double getMoney() {
        Double amount = 0d;
        for (Transaction tr : new ArrayList<>(transactions)) {
            if (tr.getUuidFrom().equalsIgnoreCase(uuid.toString())) {
                amount -= tr.getAmount();
            } else if (tr.getUuidTo().equalsIgnoreCase(uuid.toString())) {
                amount += tr.getAmount();
            }
        }
        return amount;
    }

    public Long getLastTransactionSave() {
        Long output = -1l;
        ArrayList<Transaction> transactionsCopy = new ArrayList<>(transactions);
        for (Transaction tr : transactionsCopy) {
            output = Long.max(tr.getSave(), output);
        }
        return output;
    }

    public long getBanTime() {
        return banTime;
    }

    public void syncScoreboard(Player p) {
        try {

            peScoreboard = new PeScoreboard(p);


            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date d = new Date();
            DecimalFormat df = new DecimalFormat("0.###");
            PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
            PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);

            if (sbShowHourAndLag) {
                peScoreboard.setEntry("titre_heure", "§5-*-*-*§eDate et lag§5*-*-*-", "");
                peScoreboard.setEntry("Heure", "[H]", sdf.format(d));
                try {
                    peScoreboard.setEntry("Lag", "[Lag]", StringUtils.getLagColor(100 - (Lag.getTPS() * 5)) + df.format(100 - (Lag.getTPS() * 5)) + "%");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (sbShowLocationInformation) {
                peScoreboard.setEntry("titre_loc", "§5-*-*-*§eEmplacement§5*-*-*-", "");
                peScoreboard.setEntry("Home_Dist", "[dHome]", "XXX m");
                String allianceLocation = "§eZone Hostile";
                //TODO Optimize to do only one call if the chunk hasnt changed
                AbstractMap.SimpleEntry<PeAlliance, PeAllianceFlag> allianceAndFlag = AllianceManager.INSTANCE.getFlagByCoordinates(p.getLocation().getChunk().getX(), p.getLocation().getChunk().getZ());
                if (allianceAndFlag != null) {
                    PeAlliance allianceTerr = allianceAndFlag.getKey();
                    PeAllianceFlag allianceFlag = allianceAndFlag.getValue();
                    if (allianceTerr == alliance) {
                        allianceLocation = "§a" + allianceFlag.getFlagType().getTranslatedName();
                    } else {
                        allianceLocation = "§4" + allianceTerr.getName() + " " + allianceFlag.getFlagType().name();
                    }
                }
                peScoreboard.setEntry("[T]", "[T]", allianceLocation);
                int pX = p.getLocation().getBlockX();
                int pY = p.getLocation().getBlockY();
                int pZ = p.getLocation().getBlockZ();
                peScoreboard.setEntry("coords", "[X/Y/Z]", pX + "/" + pY + "/" + pZ);
                peScoreboard.setEntry("Biome", "[Biome]", p.getLocation().getWorld().getBiome(pX, pY, pZ).name());
            }
            if (sbShowEvent) {
                peScoreboard.setEntry("titre_event", "§5-*-*-*§eEvents§5*-*-*-", "");
                peScoreboard.setEntry("event1", "", "Aucun event en cours");
                peScoreboard.setEntry("event2", "", "");
                peScoreboard.setEntry("event3", "", "");
            }
            if (sbShowAlliance && alliance != null) {
                peScoreboard.setEntry("titre_alliance", "§5-*-*-*§eAlliance§5*-*-*-", "");
                peScoreboard.setEntry("Alliance", "[A]", alliance.getName());
                peScoreboard.setEntry("WaitingDemands", "[Attente]", "" + alliance.getWaitingApproval().size());
                peScoreboard.setEntry("ValeurAlliance", "[Valeur]", "0 px");
            }

            peScoreboard.setScoreboard(p);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void initPePlayerDataList() {
        pePlayerData = new ArrayList<>();
    }
}
