package org.dudullle.pixels.evolved.survival.utils;

import org.bukkit.Bukkit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static final FileUtils INSTANCE = new FileUtils();

    private FileUtils() {

    }

    public void saveFile(String path, List<String> text) {
        PrintWriter writer;
        try {
            writer = new PrintWriter(new FileWriter(path));
            for (String str : text) {
                writer.println(str);

            }
            writer.flush();
            writer.close();
        } catch (Exception e) {
            Bukkit.getLogger().severe("COULD NOT WRITE TO " + path + " : " + e.getLocalizedMessage());
        }
    }

    public List<String> readAllText(String path) {
        List<String> txt = new ArrayList<>();
        try {
            InputStream ips = new FileInputStream(path);
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            while ((ligne = br.readLine()) != null) {
                txt.add(ligne);
            }
            br.close();
        } catch (Exception e) {
            Bukkit.getLogger().severe("COULD NOT READ FROM " + path + " : " + e.getLocalizedMessage());
        }
        return txt;
    }
}
