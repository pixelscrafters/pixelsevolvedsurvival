package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.database.WarpManager;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.utils.TeleportUtils;
import org.dudullle.pixels.evolved.survival.warps.WarpData;

import java.util.Map;
import java.util.TreeMap;

public class WarpCommand extends AbstractCommand {
    public static final WarpCommand INSTANCE = new WarpCommand();
    public static final String COMMAND_NAME = "warp";

    private WarpCommand() {
        super(COMMAND_NAME);
        Map<String, String> subCommandDescription = new TreeMap<>();
        subCommandDescription.put("", "Cette aide");
        subCommandDescription.put("[NAME]", "Téléporte au warp voulu");
        subCommandDescription.put("list", "Liste les warps");
        subCommandDescription.put("set [NAME]", "Permet de configurer un warp");
        this.setSubCommandDescription(subCommandDescription);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (args.length < 1) {
                for (String helpLine : this.getHelpText()) {
                    sender.sendMessage(helpLine);
                }
            } else {
                String firstArg = args[0];
                if (firstArg.equalsIgnoreCase("list")) {
                    if (p.hasPermission("pe.warps.list")) {
                        p.sendMessage("§5Liste des warps :");
                        for (WarpData warpData : WarpManager.INSTANCE.getWarpData()) {
                            p.sendMessage("§5-> §d" + warpData.getWarpName());
                        }
                    } else {
                        p.sendMessage("§cVous n'avez pas la permission.");
                    }
                } else if (firstArg.equalsIgnoreCase("set")) {
                    if (p.hasPermission("pe.warps.set")) {
                        if (args.length != 2) {
                            p.sendMessage("§cMerci de spécifier un nom.");
                        } else {
                            WarpData old = WarpManager.INSTANCE.getWarpByName(args[1]);
                            if (old != null) {
                                WarpData newWarpData = new WarpData(old.getId(), old.getWarpName(), new AlliancePointOfInterest(p.getLocation()));
                                WarpManager.INSTANCE.updateWarpData(old, newWarpData);
                                p.sendMessage("Opération réussie");
                            } else {
                                WarpData newWarpData = new WarpData(-1, args[1], new AlliancePointOfInterest(p.getLocation()));
                                WarpManager.INSTANCE.addWarpData(newWarpData);
                                p.sendMessage("Opération réussie");
                            }
                        }
                    } else {
                        p.sendMessage("§cVous n'avez pas la permission.");
                    }
                } else {
                    WarpData warp = WarpManager.INSTANCE.getWarpByName(args[0]);
                    if (warp != null) {
                        TeleportUtils.INSTANCE.teleportWithDelay(warp.getLocation(), null, p);
                    } else {
                        p.sendMessage("§cLe warp n'a pas été trouvé.");
                    }
                }
            }
        } else {
            sender.sendMessage("La console ne peut effectuer cette action.");
        }
        return true;
    }
}
