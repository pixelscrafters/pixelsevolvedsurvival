package org.dudullle.pixels.evolved.survival.listeners;

import com.destroystokyo.paper.Title;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.alliances.AllianceManager;
import org.dudullle.pixels.evolved.survival.alliances.PeAlliance;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlag;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlagObject;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.listeners.customevents.EndOfTimeEvent;
import org.dudullle.pixels.evolved.survival.money.MoneyManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.DrawUtils;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;


public class PlayerListener implements Listener {
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        List<String> words = Arrays.asList(event.getMessage().toUpperCase().split(" "));
        Player p = event.getPlayer();
        if (words.contains("EZ")) {
            p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1000, 1);
            p.sendTitle(new Title("§4§lNOOOOOOB", "GG WP NOOOOOOOB", 10, 1000, 10));
            Bukkit.getScheduler().runTask(PluginMain.getInstance(), new Runnable() {
                @Override
                public void run() {
                    p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10000, 10));
                }
            });
        } else if (words.contains("NOOB")) {
            p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1000, 1);
            p.sendTitle(new Title("§4§lEZZZZZZZZZZ", "GG WP EEEEEEEEZZZZZZZZ", 10, 1000, 10));
            Bukkit.getScheduler().runTask(PluginMain.getInstance(), new Runnable() {
                @Override
                public void run() {
                    p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10000, 10));
                }
            });
        }
        //Format chat text
        PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
        Boolean isStaff = p.hasPermission("pe.chat.staff");
        String rank = isStaff ? "§cStaff" : "§7Joueur";
        String allianceName = "§7Sans alliance";
        if (pePlayer != null) {
            PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
            if (alliance != null) {
                allianceName = "§d" + alliance.getName();
            }
        }
        event.setFormat(rank + " " + allianceName + "§5 " + p.getDisplayName() + (isStaff ? "§d " : "§e ") + event.getMessage());
    }

    @EventHandler
    public void onPlayerBlockPlace(BlockPlaceEvent event) {
        if (PeAllianceFlagObject.instanceOf(event.getItemInHand())) {
            if (((PluginMain) PluginMain.getInstance()).isLobby()) {
                event.setCancelled(true);
                event.getPlayer().sendMessage("Impossible de claim au lobby.");
                return;
            }
            event.setCancelled(true);
            //Récupération de l'alliance
            Player p = event.getPlayer();
            PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
            PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
            if (alliance != null) {
                if (alliance.isAdmin(pePlayer)) {
                    int xLocation = event.getBlockPlaced().getLocation().getChunk().getX();
                    int zLocation = event.getBlockPlaced().getLocation().getChunk().getZ();

                    AbstractMap.SimpleEntry<PeAlliance, PeAllianceFlag> alliancePeAllianceFlagSimpleEntry = AllianceManager.INSTANCE.getFlagByCoordinates(xLocation, zLocation);
                    PeAllianceFlagObject flagItem = new PeAllianceFlagObject(event.getItemInHand());
                    if (alliancePeAllianceFlagSimpleEntry == null || flagItem.getFlagType() == PeAllianceFlag.FlagType.UNCLAIM) {
                        if ((flagItem.getFlagType() != PeAllianceFlag.FlagType.UNCLAIM && flagItem.getFlagType().equals(PeAllianceFlag.FlagType.MASTER) || alliance.touchesFlag(xLocation, zLocation))) {
                            PeAllianceFlag flag = new PeAllianceFlag(xLocation, zLocation, p.getLocation().getWorld().getName(), flagItem.getFlagType());
                            alliance.addFlag(flag);
                            ItemStack it = p.getInventory().getItemInMainHand();
                            if (it.getAmount() > 1) {
                                it.setAmount(it.getAmount() - 1);
                            } else {
                                p.getInventory().getItemInMainHand().setAmount(0);
                            }

                            AllianceManager.INSTANCE.createWorldGuardRegion(alliance, xLocation, zLocation, flag, p.getWorld());
                            p.sendTitle(new Title("§aClaim effectué", "Vous avez claim le chunk", 10, 100, 10));
                        } else if (flagItem.getFlagType() == PeAllianceFlag.FlagType.UNCLAIM) {
                            PeAllianceFlag flagToRemove = alliance.getFlagAt(xLocation, zLocation);
                            if (flagToRemove == null) {
                                p.sendMessage("§cCette zone n'est pas claim");
                            } else if (!alliance.isAdmin(pePlayer)) {
                                p.sendMessage("§cVous n'êtes pas admin de l'alliance :P");
                            } else {
                                EconomyResponse er = MoneyManager.INSTANCE.withdrawPlayer(p, flagToRemove.getRemovalCosts());
                                if (er.type == EconomyResponse.ResponseType.SUCCESS) {
                                    alliance.removeFlag(flagToRemove, false);
                                    p.sendMessage("Zone unclaim !");
                                    p.getInventory().remove(p.getItemOnCursor().asOne());
                                } else {
                                    p.sendMessage("§5Vous n'avez pas assez d'argent.");
                                    p.sendMessage("§cIl vous faut §d" + flagToRemove.getRemovalCosts() + "§5 px.");
                                }
                            }
                        } else {
                            p.sendMessage("Vous devez claim a côté d'un claim principal.");
                        }
                    } else {
                        p.sendMessage("Cette zone est déjà claim.");
                    }

                } else {
                    p.sendMessage("§4Vous ne pouvez pas claim car vous n'êtes pas admin de votre alliance");
                }
            } else {
                p.sendMessage("§4Vous ne pouvez pas claim car vous n'avez pas d'alliance");
            }
        }
    }

    @EventHandler
    public void onDoClaimTick(EndOfTimeEvent event) {
        Player p = event.getPlayer();
        if (event.getAlliance() != null) {
            Block block = p.getTargetBlock(20);
            Chunk chunk = block != null ? block.getChunk() : null;
            if (chunk != null) {
                DrawUtils.INSTANCE.drawChunk(Particle.HEART, chunk, p);
                //Draw ALLIANCE FLAGS
                for (PeAllianceFlag flag : event.getAlliance().getFlags()) {
                    Chunk flagChunk = p.getWorld().getChunkAt(flag.getxLocation(), flag.getzLocation());
                    DrawUtils.INSTANCE.drawChunk(Particle.BARRIER, flagChunk, p);
                }
            }
        } else {
            p.playSound(p.getLocation(), Sound.ENTITY_ENDER_DRAGON_GROWL, 100, 0);
            p.sendTitle("§4Vous n'avez pas d'alliance", "§cVous ne pouvez pas utiliser cet item.", 10, 100, 10);
        }
    }

    @EventHandler
    public void onWorldChange(PlayerTeleportEvent event) {
        Location l1 = event.getFrom();
        Location l2 = event.getTo();
        if (!l1.getWorld().equals(l2.getWorld())) {
            event.setCancelled(true);
            event.getPlayer().sendMessage("§cCette action n'est pas possible / autorisée.");
        }
        l1.getWorld().spawnParticle(Particle.DRAGON_BREATH, l1, 1);
    }
}
