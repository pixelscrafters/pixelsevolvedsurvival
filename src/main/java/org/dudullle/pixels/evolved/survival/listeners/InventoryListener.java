package org.dudullle.pixels.evolved.survival.listeners;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.InventoryView;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlagObject;
import org.dudullle.pixels.evolved.survival.commands.AllianceCommand;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

public class InventoryListener implements Listener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        CallbackType callbackType = null;
        InventoryView inv = event.getView();
        Player p = (Player) event.getWhoClicked();
        PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
        pePlayer.setUsingClaimItem(PeAllianceFlagObject.instanceOf(p.getInventory().getItemInMainHand()));
        if (inv.getTitle().startsWith("ALLIANCE")) {
            event.setCancelled(true);
            callbackType = AllianceCommand.INSTANCE.handleInventoryClick(event, pePlayer);
        }

        if (callbackType != null) {
            handleCallbacks(p, callbackType);
        }
    }

    @EventHandler
    public void itemChange(PlayerItemHeldEvent event) {
        Player p = event.getPlayer();
        PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
        if (pePlayer != null) {
            pePlayer.setUsingClaimItem(PeAllianceFlagObject.instanceOf(p.getInventory().getItem(event.getNewSlot())));
        }
    }

    private void handleCallbacks(Player p, CallbackType callbackType) {
        switch (callbackType) {
            case BEEP_NORMAL:
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_GUITAR, 100, 0);
                break;
            case BEEP_ERROR:
                p.playSound(p.getLocation(), Sound.ENTITY_ENDER_DRAGON_GROWL, 100, 0);
                break;
        }
    }

    public enum CallbackType {
        BEEP_NORMAL,
        BEEP_ERROR,
    }
}
