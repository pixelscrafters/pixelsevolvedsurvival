package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.command.CommandExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractCommand implements CommandExecutor {
    public final String commandName;
    private Map<String, String> subCommandDescription;

    public AbstractCommand(String commandName) {
        this.commandName = commandName;
    }

    public void setSubCommandDescription(Map<String, String> subCommandDescription) {
        this.subCommandDescription = subCommandDescription;
    }

    public List<String> getHelpText() {
        List<String> outList = new ArrayList<>();
        StringBuilder output = new StringBuilder();
        output.append("§b----- §dAide de la commande §5");
        output.append(commandName);
        output.append(" §b-----");
        outList.add(output.toString());
        if (subCommandDescription != null) {
            for (String subCommand : subCommandDescription.keySet()) {
                outList.add("§b§l-/" + commandName + " " + subCommand + " : §b" + subCommandDescription.get(subCommand));
            }
        }
        return outList;
    }
}
