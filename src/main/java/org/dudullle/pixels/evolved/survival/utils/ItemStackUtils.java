package org.dudullle.pixels.evolved.survival.utils;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;

public class ItemStackUtils {

    public static ItemStack get(String itemName, Material material, List<String> lore) {
        ItemStack i = new ItemStack(material, 1);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(itemName);
        m.setLore(lore);
        i.setItemMeta(m);
        return i;
    }

    public static ItemStack get(String itemName, Material material, String... lore) {
        List<String> loreList = Arrays.asList(lore);
        return get(itemName, material, loreList);
    }

    public static ItemStack get(Material Mat, List<String> lore) {
        ItemStack i = new ItemStack(Mat, 1);

        ItemMeta m = i.getItemMeta();
        m.setLore(lore);
        i.setItemMeta(m);
        return i;
    }

    public static ItemStack get(String ItemName, Material Mat) {
        ItemStack i = new ItemStack(Mat, 1);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(ItemName);
        i.setItemMeta(m);
        return i;

    }

    public static ItemStack get(Material Mat) {
        ItemStack i = new ItemStack(Mat, 1);
        return i;

    }

    public static ItemStack get(Material Mat, int qte) {
        ItemStack i = new ItemStack(Mat, 1);
        i.setAmount(qte);
        return i;

    }

    public static ItemStack getHead(Player player) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        skull.setDisplayName(player.getName());
        skull.setOwningPlayer(player);
        item.setItemMeta(skull);
        return item;
    }

    public static ItemStack getHead(OfflinePlayer player, String... lore) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);
        List<String> loreList = Arrays.asList(lore);
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        skull.setDisplayName(player.getName());
        skull.setOwningPlayer(player);
        skull.setLore(loreList);
        item.setItemMeta(skull);
        return item;
    }

    public static ItemStack getHead(OfflinePlayer player, List<String> loreList) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        skull.setDisplayName(player.getName());
        skull.setOwningPlayer(player);
        skull.setLore(loreList);
        item.setItemMeta(skull);
        return item;
    }


    public static ItemStack getHead(String pName, String itemName) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        skull.setDisplayName(itemName);
        skull.setOwner(pName);
        item.setItemMeta(skull);
        return item;
    }
}
