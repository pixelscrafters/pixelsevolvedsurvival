package org.dudullle.pixels.evolved.survival.warps;

import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;

public class WarpData {
    private final long id;
    private final String warpName;
    private final AlliancePointOfInterest location;

    public WarpData(long id, String warpName, AlliancePointOfInterest location) {
        this.id = id;
        this.warpName = warpName;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public String getWarpName() {
        return warpName;
    }

    public AlliancePointOfInterest getLocation() {
        return location;
    }
}
