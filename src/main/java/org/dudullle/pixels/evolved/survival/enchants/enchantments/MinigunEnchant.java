package org.dudullle.pixels.evolved.survival.enchants.enchantments;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.dudullle.pixels.evolved.survival.enchants.AEnchant;

public class MinigunEnchant extends AEnchant {

    public MinigunEnchant() {
        super(NamespacedKey.minecraft("MINIGUN"));
    }

    @Override
    public void handleEvent(EnchantInterractType action, Player p, Event e) {
        if (action.equals(EnchantInterractType.SHOOT_BOW)) {

        }
    }

    @Override
    public String getName() {
        return "Minigun";
    }

    @Override
    public int getMaxLevel() {
        return 1;
    }

    @Override
    public int getStartLevel() {
        return 1;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return EnchantmentTarget.BOW;
    }

    @Override
    public boolean isTreasure() {
        return false;
    }

    @Override
    public boolean isCursed() {
        return false;
    }

    @Override
    public boolean conflictsWith(Enchantment other) {
        return false;
    }

    @Override
    public boolean canEnchantItem(ItemStack item) {
        return false;
    }
}
