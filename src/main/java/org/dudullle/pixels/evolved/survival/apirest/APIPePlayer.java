package org.dudullle.pixels.evolved.survival.apirest;

import com.google.common.base.Strings;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.bukkit.Bukkit;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class APIPePlayer extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String stringPlayerUUID = req.getParameter("playerUUID");
        Bukkit.getLogger().fine("REQUEST DATA WITH " + stringPlayerUUID);
        APIReturnObject returnMessage = new APIReturnObject(ReturnMessage.ERROR_EMPTY, "Erreur inconnue.");
        if (!Strings.isNullOrEmpty(stringPlayerUUID)) {
            try {
                UUID playerUUID = UUID.fromString(stringPlayerUUID);
                PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(playerUUID);
                returnMessage = new APIReturnObject(ReturnMessage.SUCCESS, pePlayer);
            } catch (IllegalArgumentException ex) {
                returnMessage = new APIReturnObject(ReturnMessage.ERROR_NOT_EXISTS, "UUID invalide");
            }
        } else {
            List<PePlayer> players = PlayersManager.INSTANCE.getPlayers();
            returnMessage = new APIReturnObject(ReturnMessage.SUCCESS, players);
        }
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getOutputStream().println(returnMessage.toString());
    }
}
