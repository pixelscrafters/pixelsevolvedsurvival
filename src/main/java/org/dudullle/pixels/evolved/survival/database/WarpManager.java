package org.dudullle.pixels.evolved.survival.database;

import org.bukkit.Bukkit;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.warps.WarpData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class WarpManager {
    public static final WarpManager INSTANCE = new WarpManager();
    private final List<WarpData> warpData;
    private final Queue<WarpData> toSave = new LinkedBlockingQueue<>();
    private final Queue<WarpData> toDelete = new LinkedBlockingQueue<>();

    private WarpManager() {
        warpData = new ArrayList<>();
    }

    public void readAll() {
        Connection connection = null;
        try {
            connection = DbManager.INSTANCE.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM `warps`");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                long id = rs.getLong("id");
                String warpName = rs.getString("warp_name");
                String location = rs.getString("warp_location");
                AlliancePointOfInterest warpLocation = AlliancePointOfInterest.fromJson(location);
                this.warpData.add(new WarpData(id, warpName, warpLocation));
            }
            Bukkit.getLogger().info("Warps chargés : " + this.warpData.size());
        } catch (SQLException throwables) {
            Bukkit.getLogger().severe("Erreur de SQL au démarrage. " + throwables.getLocalizedMessage());
        } finally {
            if (connection != null) {
                try {
                    DbManager.INSTANCE.closeConnection(connection);
                } catch (SQLException throwables) {
                    Bukkit.getLogger().severe("Erreur de déconnexion SQL au démarrage.");
                }
            }
        }
    }

    public void write() {
        if (!this.toSave.isEmpty()) {
            WarpData warpData = this.toSave.poll();
            Connection connection = null;
            try {
                connection = DbManager.INSTANCE.openConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `warps` (`id`, `warp_name`, `warp_location`) VALUES (NULL, ?, ?);");
                preparedStatement.setString(1, warpData.getWarpName());
                preparedStatement.setString(2, warpData.getLocation().toString());
                preparedStatement.executeUpdate();
            } catch (SQLException throwables) {
                Bukkit.getLogger().severe("Erreur de SQL au WARP WRITE. " + throwables.getLocalizedMessage());
            } finally {
                if (connection != null) {
                    try {
                        DbManager.INSTANCE.closeConnection(connection);
                    } catch (SQLException throwables) {
                        Bukkit.getLogger().severe("Erreur de déconnexion SQL au WARP WRITE.");
                    }
                }
            }
        }
    }

    public void delete() {
        if (!this.toDelete.isEmpty()) {
            WarpData warpData = this.toDelete.poll();
            Connection connection = null;
            try {
                connection = DbManager.INSTANCE.openConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `warps` WHERE `warps`.`warp_name` LIKE ? AND `warps`.`warp_location` LIKE ?");
                preparedStatement.setString(1, warpData.getWarpName());
                preparedStatement.setString(2, warpData.getLocation().toString());
                preparedStatement.executeUpdate();
            } catch (SQLException throwables) {
                Bukkit.getLogger().severe("Erreur de SQL WARP DEL " + throwables.getLocalizedMessage());
            } finally {
                if (connection != null) {
                    try {
                        DbManager.INSTANCE.closeConnection(connection);
                    } catch (SQLException throwables) {
                        Bukkit.getLogger().severe("Erreur de déconnexion SQL WARP DEL.");
                    }
                }
            }
        }

    }

    public WarpData getWarpByName(String name) {
        return this.warpData.stream().filter(x -> x.getWarpName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public void addWarpData(WarpData warpData) {
        this.warpData.add(warpData);
        this.toSave.add(warpData);
    }

    public void rmvWarpData(WarpData warpData) {
        this.warpData.remove(warpData);
        this.toDelete.add(warpData);
    }

    public void updateWarpData(WarpData warpDataOld, WarpData warpDataNew) {
        this.toDelete.add(warpDataOld);
        this.toSave.add(warpDataNew);
        this.warpData.remove(warpDataOld);
        this.warpData.add(warpDataNew);
    }

    public final List<WarpData> getWarpData() {
        return new ArrayList<>(this.warpData);
    }
}
