package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.utils.TeleportUtils;

public class TpyCommand extends AbstractCommand {
    public static final String COMMAND_NAME = "tpy";
    public static final TpyCommand INSTANCE = new TpyCommand();

    private TpyCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player destination = (Player) sender;
            PePlayer peDest = PlayersManager.INSTANCE.getPePlayer(destination.getUniqueId());
            if (peDest != null && peDest.getUuidWaitingForTp() != null) {
                Player source = Bukkit.getPlayer(peDest.getUuidWaitingForTpUuid());
                if (source != null && source.isOnline()) {
                    peDest.sendMessage("§aDemande acceptée.");
                    source.sendMessage(peDest.getName() + " §ca accepté votre demande. §ENE BOUGEZ PAS :)");
                    peDest.setUuidWaitingForTp(null);
                    TeleportUtils.INSTANCE.teleportWithDelay(new AlliancePointOfInterest(destination.getLocation()), peDest, source);
                } else {
                    peDest.sendMessage("§cLe joueur est introuvable");
                }
            } else {
                peDest.sendMessage("Vous n'avez pas de demande en attente.");
            }
        } else {
            sender.sendMessage("§cLa console ne se téléporte pas");
        }
        return true;
    }
}
