package org.dudullle.pixels.evolved.survival.permissions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.database.DbManager;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class PermissionManager {
    public static final PermissionManager INSTANCE = new PermissionManager();
    private final Map<String, PermissionGroup> permissionGroupMap = new TreeMap<>();
    private final Map<UUID, PermissionAttachment> affectedPermissions = new TreeMap<>();
    private final Set<PermissionGroup> removeQueue = new HashSet<>();
    private final Set<PermissionGroup> saveQueue = new HashSet<>();
    private final Set<PermissionGroup> saveNewQueue = new HashSet<>();

    private PermissionManager() {

    }

    public void loadService() {
        //Premier chargement !
        Connection connection = null;
        try {
            connection = DbManager.INSTANCE.openConnection();
            Bukkit.getLogger().info("Chargement des Permissions depuis la base.");
            loadAllPerms(connection);
        } catch (SQLException throwables) {
            Bukkit.getLogger().severe("Erreur de SQL au démarrage. " + throwables.getLocalizedMessage());
        } finally {
            if (connection != null) {
                try {
                    DbManager.INSTANCE.closeConnection(connection);
                } catch (SQLException throwables) {
                    Bukkit.getLogger().severe("Erreur de déconnexion SQL au démarrage.");
                }
            }
        }
    }

    public List<String> getPermsByGroupName(String groupName) {
        PermissionGroup permGroup = permissionGroupMap.get(groupName);
        if (permGroup == null) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(permGroup.getPermissions());
        }
    }

    public List<String> getGroupNames() {
        List<String> ret = new ArrayList<>();
        for (PermissionGroup group : permissionGroupMap.values()) {
            ret.add(group.getName());
        }
        return ret;
    }

    public void refresh() {
        //Called every 30 mins or with the /peperm reload
        Connection connection = null;
        try {
            connection = DbManager.INSTANCE.openConnection();
            Bukkit.getLogger().info("Chargement des Permissions depuis la base.");
            if (((PluginMain) PluginMain.getInstance()).isLobby()) {
                for (PermissionGroup group : saveNewQueue) {
                    insertGroupToDatabase(connection, group);
                }
                saveNewQueue.clear();

                for (PermissionGroup group : saveQueue) {
                    updateGroupToDatabase(connection, group);
                }
                saveQueue.clear();

                for (PermissionGroup group : removeQueue) {
                    removeGroupToDatabase(connection, group);
                }
                removeQueue.clear();
            } else {
                Bukkit.getLogger().info("[PE] Saving permissions is disabled on this server.");
            }

            loadAllPerms(connection);
        } catch (SQLException throwables) {
            Bukkit.getLogger().severe("Erreur de SQL au démarrage. " + throwables.getLocalizedMessage());
        } finally {
            if (connection != null) {
                try {
                    DbManager.INSTANCE.closeConnection(connection);
                } catch (SQLException throwables) {
                    Bukkit.getLogger().severe("Erreur de déconnexion SQL au démarrage.");
                }
            }
        }
    }

    public void loadAllPerms(Connection conn) throws SQLException {
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery("SELECT * FROM permissions");
        permissionGroupMap.clear();
        while (rs.next()) {
            PermissionGroup permissionGroup = PermissionGroup.fromJson(rs.getString("json_value"));
            permissionGroupMap.put(permissionGroup.getName(), permissionGroup);
            Bukkit.getLogger().info("[PE] Loaded permission group " + permissionGroup.getName());
        }

        if (!permissionGroupMap.containsKey("DEFAULT")) {
            permissionGroupMap.put("DEFAULT", new PermissionGroup("DEFAULT", new ArrayList<>(), new ArrayList<>(), true));
            saveNewQueue.add(permissionGroupMap.get("DEFAULT"));
        }
    }

    public void updateGroupToDatabase(Connection conn, PermissionGroup permissionGroup) throws SQLException {
        Bukkit.getLogger().info("Updating permission to database " + permissionGroup.getName());
        PreparedStatement stm = conn.prepareStatement("UPDATE `permissions` SET `json_value` = ? WHERE `permissions`.`group_name` = ?; ");
        stm.setString(1, permissionGroup.toString());
        stm.setString(2, permissionGroup.getName());
        stm.executeUpdate();
    }

    public void insertGroupToDatabase(Connection conn, PermissionGroup permissionGroup) throws SQLException {
        Bukkit.getLogger().info("Inserting permission to database " + permissionGroup.getName());
        PreparedStatement stm = conn.prepareStatement("INSERT INTO `permissions` (`id`, `group_name`, `json_value`) VALUES (NULL, ?, ?);");
        stm.setString(1, permissionGroup.getName());
        stm.setString(2, permissionGroup.toString());
        stm.executeUpdate();
    }

    public void removeGroupToDatabase(Connection conn, PermissionGroup permissionGroup) throws SQLException {
        Bukkit.getLogger().info("Delete permission to database " + permissionGroup.getName());
        PreparedStatement stm = conn.prepareStatement("DELETE FROM `permissions` WHERE `permissions`.`group_name` = ?;");
        stm.setString(1, permissionGroup.getName());
        stm.executeUpdate();
    }

    public ReturnMessage addPermissionToGroup(String groupName, String permission) {
        PermissionGroup permGroup = permissionGroupMap.get(groupName);
        if (permGroup == null) {
            return ReturnMessage.ERROR_NOT_EXISTS;
        } else if (permGroup.getPermissions().contains(permission)) {
            return ReturnMessage.ERROR_ALREADY_MEMBER;
        }
        permGroup.addPermission(permission);
        saveQueue.add(permGroup);
        return ReturnMessage.SUCCESS;
    }

    public ReturnMessage rmvPermissionToGroup(String groupName, String permission) {
        PermissionGroup permGroup = permissionGroupMap.get(groupName);
        if (permGroup == null || !permGroup.getPermissions().contains(permission)) {
            return ReturnMessage.ERROR_NOT_EXISTS;
        }
        permGroup.rmvPermission(permission);
        saveQueue.add(permGroup);
        return ReturnMessage.SUCCESS;
    }

    public ReturnMessage addPlayerToGroup(String groupName, Player p) {
        PermissionGroup permGroup = permissionGroupMap.get(groupName);
        if (permGroup == null) {
            return ReturnMessage.ERROR_NOT_EXISTS;
        }
        if (permGroup.getMembers().contains(p.getUniqueId())) {
            return ReturnMessage.ERROR_ALREADY_MEMBER;
        }
        permGroup.addMember(p.getUniqueId());
        setGroupPermissionsOfPlayer(p.getUniqueId(), permGroup);
        saveQueue.add(permGroup);
        return ReturnMessage.SUCCESS;
    }

    public ReturnMessage rmvPlayerToGroup(String groupName, Player p) {
        PermissionGroup permGroup = permissionGroupMap.get(groupName);
        if (!permGroup.getMembers().contains(p.getUniqueId())) {
            return ReturnMessage.ERROR_NOT_EXISTS;
        }
        permGroup.removeMember(p.getUniqueId());
        unsetGroupPermissionsOfPlayer(p.getUniqueId(), permGroup);
        saveQueue.add(permGroup);
        return ReturnMessage.SUCCESS;
    }

    public ReturnMessage addGroup(String groupName) {
        if (permissionGroupMap.containsKey(groupName)) {
            return ReturnMessage.ALREADY_EXISTS;
        }
        PermissionGroup permissionGroup = new PermissionGroup(groupName, new ArrayList<>(), new ArrayList<>(), false);
        permissionGroupMap.put(groupName, permissionGroup);
        saveNewQueue.add(permissionGroup);
        return ReturnMessage.SUCCESS;
    }

    public ReturnMessage rmvGroup(String groupName) {
        if (!permissionGroupMap.containsKey(groupName)) {
            return ReturnMessage.ERROR_NOT_EXISTS;
        }
        PermissionGroup permGroup = permissionGroupMap.remove(groupName);
        for (UUID player : permGroup.getMembers()) {
            unsetGroupPermissionsOfPlayer(player, permGroup);
        }
        removeQueue.add(permGroup);
        return ReturnMessage.SUCCESS;
    }

    public void setPermissionsOfPlayer(Player player) {
        PermissionAttachment permissionAttachment;
        if (affectedPermissions.containsKey(player.getUniqueId())) {
            permissionAttachment = affectedPermissions.get(player.getUniqueId());
        } else {
            permissionAttachment = player.addAttachment(PluginMain.getInstance());
            affectedPermissions.put(player.getUniqueId(), permissionAttachment);
        }
        List<PermissionGroup> groups = permissionGroupMap.values().stream().filter(x -> x != null && (x.isDefault || x.getMembers().contains(player.getUniqueId()))).collect(Collectors.toList());
        for (PermissionGroup permissionGroup : groups) {
            permissionGroup.getPermissions().forEach(x -> permissionAttachment.setPermission(x, true));
        }
    }

    public void unsetGroupPermissionsOfPlayer(UUID player, PermissionGroup group) {
        PermissionAttachment permissionAttachment = affectedPermissions.get(player);
        group.getPermissions().forEach(x -> permissionAttachment.unsetPermission(x));
        affectedPermissions.put(player, permissionAttachment);
    }

    public void setGroupPermissionsOfPlayer(UUID player, PermissionGroup group) {
        PermissionAttachment permissionAttachment = affectedPermissions.get(player);
        group.getPermissions().forEach(x -> permissionAttachment.setPermission(x, true));
        affectedPermissions.put(player, permissionAttachment);
    }

    public void removeAttachment(Player player) {
        if (affectedPermissions.containsKey(player.getUniqueId())) {
            player.removeAttachment(affectedPermissions.get(player.getUniqueId()));
        }
        affectedPermissions.remove(player.getUniqueId());
    }
}
