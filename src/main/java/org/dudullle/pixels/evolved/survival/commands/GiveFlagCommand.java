package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlag;
import org.dudullle.pixels.evolved.survival.alliances.PeAllianceFlagObject;

import java.util.Map;
import java.util.TreeMap;

public class GiveFlagCommand extends AbstractCommand {
    public static final GiveFlagCommand INSTANCE = new GiveFlagCommand();
    public static final String COMMAND_NAME = "giveFlag";

    public GiveFlagCommand() {
        super(COMMAND_NAME);
        Map<String, String> subCommandDescription = new TreeMap<>();
        subCommandDescription.put("help", "DEV COMMAND");
        this.setSubCommandDescription(subCommandDescription);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            String arg = args.length > 0 ? args[0] : "NULL";
            if (!arg.equals("GET")) {
                PeAllianceFlag.FlagType flagType = PeAllianceFlag.FlagType.valueOf(arg);
                p.getInventory().addItem(new PeAllianceFlagObject(flagType));
            } else {
                ItemStack it = p.getInventory().getItemInMainHand();
                if (PeAllianceFlagObject.instanceOf(it)) {
                    p.sendMessage("CLASS OK");
                    p.sendMessage("OTHER DATA : " + new PeAllianceFlagObject(it).getFlagType().name());
                } else {
                    p.sendMessage("CLASS NOT OK");
                }
            }
        }
        return false;
    }
}
