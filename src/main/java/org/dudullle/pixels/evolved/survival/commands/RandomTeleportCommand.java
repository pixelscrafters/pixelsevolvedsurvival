package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;
import org.dudullle.pixels.evolved.survival.utils.TeleportUtils;

import java.util.Random;

public class RandomTeleportCommand extends AbstractCommand {
    public static final String COMMAND_NAME = "rtp";
    public static final RandomTeleportCommand INSTANCE = new RandomTeleportCommand();

    private RandomTeleportCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length != 1) {
                player.sendMessage("§dNahaaa ! Cette fonctionnalité est dispo pour les PNJs et ceux qui connaissent le mot de passe ... /rtp <MDP>");
            } else if (args[0].equalsIgnoreCase("dudullle")) {
                Random random = new Random();
                int x = random.nextInt(20000) - 10000;
                int z = random.nextInt(20000) - 10000;
                int y = player.getWorld().getHighestBlockYAt(x, z);

                Location randomLocation = new Location(player.getWorld(), x, y, z);
                TeleportUtils.INSTANCE.teleportWithDelay(new AlliancePointOfInterest(randomLocation), null, player);
            } else {
                player.sendMessage("§dNahaaa ! Cette fonctionnalité est dispo pour les PNJs et ceux qui connaissent le mot de passe et apparement tu le connais pas :P ... /rtp <MDP>");
            }
        } else {
            sender.sendMessage("... Très drole");
        }
        return true;
    }
}
