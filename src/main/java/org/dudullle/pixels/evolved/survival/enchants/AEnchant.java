package org.dudullle.pixels.evolved.survival.enchants;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public abstract class AEnchant extends Enchantment {
    public AEnchant(NamespacedKey key) {
        super(key);
    }

    public abstract void handleEvent(EnchantInterractType action, Player p, Event e);

    public enum EnchantItemType {
        ARMOR,
        GLOBAL,
        BOW,
        BOOTS,
        HELMET,
        TOOL
    }

    public enum EnchantInterractType {
        INTERACT,
        INTERACT_ENTITY,
        INTERACT_LEFT,
        INTERACT_RIGHT,
        MOVE,
        DAMAGE_GIVEN,
        DAMAGE_TAKEN,
        DAMAGE_NATURE,
        BLOCK_PLACED,
        BLOCK_BROKEN,
        SHOOT_BOW,
        PROJECTILE_THROWN,
        PROJECTILE_HIT,
        WEAR_ITEM,
        DEATH
    }
}
