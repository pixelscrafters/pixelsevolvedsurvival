package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;

public class HomeCommand extends AbstractCommand {
    public static final HomeCommand INSTANCE = new HomeCommand();
    public static final String COMMAND_NAME = "home";

    public HomeCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
            if (args.length == 0) {
                AlliancePointOfInterest home = pePlayer.getHome();
                if (home != null) {
                    if (p.hasPermission("pe.tp.instant")) {
                        teleport(home, pePlayer, p, null);
                    } else {
                        p.sendMessage("§dTéléportation dans 3 secondes. Ne bougez pas !");
                        Location initialLocation = p.getLocation();
                        Bukkit.getScheduler().runTaskLater(PluginMain.getInstance(), () -> {
                            teleport(home, pePlayer, p, initialLocation);
                        }, 20L * 3);
                    }
                } else {
                    p.sendMessage("§cVous n'avez pas de home. Faites §d/sethome §cpour régler votre home.");
                }
            } else if (p.hasPermission("pe.admin.tp") && args.length == 2 && args[0].equalsIgnoreCase("player")) {
                //Obtenir les homes du joueur
                String player1Name = args[1];
                OfflinePlayer player1 = Bukkit.getOfflinePlayerIfCached(player1Name);
                if (player1 != null) {
                    PePlayer pePlayer1 = PlayersManager.INSTANCE.getPePlayer(player1.getUniqueId());
                    AlliancePointOfInterest player1Home = pePlayer1.getHome();
                    if (player1Home != null) {
                        teleport(player1Home, pePlayer1, p, null);
                    } else {
                        p.sendMessage("§cLe joueur n'a pas de home.");
                    }
                } else {
                    p.sendMessage("Le joueur n'a pas été trouvé.");
                }

            } else {
                p.sendMessage("§cErreur de permission ou de commande. Vous n'êtes pas autorisé à faire ceci.");
            }
        } else {
            sender.sendMessage("§4Vous ne pouvez pas vous téléporter à un home comme ça :/... Surtout en console !");
        }
        return true;
    }

    public void teleport(AlliancePointOfInterest home, PePlayer pePlayer, Player p, Location initialLocation) {
        if (initialLocation != null && !initialLocation.equals(p.getLocation())) {
            p.sendMessage("§cTéléportation annulée. Vous avez bougé :(");
        } else if (Bukkit.getWorld(home.getWorldName()) == null) {
            p.sendMessage("§cVous devez être sur le serveur " + home.getWorldName());
        } else {
            p.teleportAsync(home.toLocation());
            p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 100, 0);
        }
    }
}
