package org.dudullle.pixels.evolved.survival.modo;

import java.util.UUID;

public class ModoMail {
    private final UUID sender;
    private final UUID receiver;
    private final String object;
    private final String content;
    private final Long sentDate;
    private Boolean hasBeenRead;

    public ModoMail(UUID sender, UUID receiver, String object, String content, Long sentDate, Boolean hasBeenRead) {
        this.sender = sender;
        this.receiver = receiver;
        this.object = object;
        this.content = content;
        this.sentDate = sentDate;
        this.hasBeenRead = hasBeenRead;
    }

    public UUID getSender() {
        return sender;
    }

    public UUID getReceiver() {
        return receiver;
    }

    public String getObject() {
        return object;
    }

    public String getContent() {
        return content;
    }

    public Long getSentDate() {
        return sentDate;
    }

    public Boolean getHasBeenRead() {
        return hasBeenRead;
    }

    public void setHasBeenRead(Boolean hasBeenRead) {
        this.hasBeenRead = hasBeenRead;
    }
}
