package org.dudullle.pixels.evolved.survival.permissions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PermissionGroup {
    public Boolean isDefault;
    private final String name;
    private List<UUID> members = new ArrayList<>();
    private List<String> permissions = new ArrayList<>();

    public PermissionGroup(String name, List<UUID> members, List<String> permissions, Boolean isDefault) {
        this.name = name;
        this.members = members;
        this.permissions = permissions;
        this.isDefault = isDefault;
    }

    public static PermissionGroup fromJson(String json) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, PermissionGroup.class);
    }

    public String getName() {
        return name;
    }

    public List<UUID> getMembers() {
        return members;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void addMember(UUID player) {
        if (members == null) {
            members = new ArrayList<>();
        }
        members.add(player);
    }

    public void removeMember(UUID player) {
        if (members == null) {
            members = new ArrayList<>();
        }
        members.remove(player);
    }

    public void addPermission(String permission) {
        if (permissions == null) {
            permissions = new ArrayList<>();
        }
        permissions.add(permission);
    }

    public void rmvPermission(String permission) {
        if (permissions == null) {
            permissions = new ArrayList<>();
        }
        permissions.remove(permission);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
