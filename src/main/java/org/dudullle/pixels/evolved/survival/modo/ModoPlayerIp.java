package org.dudullle.pixels.evolved.survival.modo;

public class ModoPlayerIp {
    private final String ip;
    private final long ipAddedDate;

    public ModoPlayerIp(String ip, long ipAddedDate) {
        this.ip = ip;
        this.ipAddedDate = ipAddedDate;
    }

    public String getIp() {
        return ip;
    }

    public long getIpAddedDate() {
        return ipAddedDate;
    }
}
