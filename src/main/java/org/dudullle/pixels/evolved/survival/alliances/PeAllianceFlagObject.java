package org.dudullle.pixels.evolved.survival.alliances;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PeAllianceFlagObject extends ItemStack {
    private static final String ITEM_NAME = "Item de claim";
    private PeAllianceFlag.FlagType flagType;


    public PeAllianceFlagObject(ItemStack itemStackOrigin) {
        super(itemStackOrigin);
        if (instanceOf(itemStackOrigin)) {
            List<String> lore = this.getLore();
            if (lore != null && lore.size() == 5) {
                this.flagType = PeAllianceFlag.FlagType.valueOf(lore.get(3));
            }
        }
    }

    public PeAllianceFlagObject(PeAllianceFlag.FlagType flagType) {
        super(flagType.getMaterial());
        List<String> lore = this.getLore();
        if (lore == null) {
            lore = new ArrayList<>();
        }
        lore.add("§d" + flagType.getTranslatedName());
        lore.add("§7" + flagType.getDescription());
        lore.add("§dVie du claim : " + PeAllianceFlag.getMaxHealth(flagType));
        lore.add(flagType.name());
        lore.add("§7" + UUID.randomUUID().toString());
        this.setLore(lore);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(ITEM_NAME);
        this.setItemMeta(meta);
    }

    public static Boolean instanceOf(ItemStack objectToTest) {
        if (objectToTest != null && objectToTest.getItemMeta() != null && objectToTest.getItemMeta().getDisplayName().equals(ITEM_NAME)) {
            List<String> lore = objectToTest.getLore();
            if (lore != null && lore.size() == 5 && lore.get(3) != null) {
                try {
                    PeAllianceFlag.FlagType flagType = PeAllianceFlag.FlagType.valueOf(lore.get(3));
                    return true;
                } catch (NullPointerException ex) {
                    return false;
                }
            }
        }
        return false;
    }

    public PeAllianceFlag.FlagType getFlagType() {
        return flagType;
    }
}
