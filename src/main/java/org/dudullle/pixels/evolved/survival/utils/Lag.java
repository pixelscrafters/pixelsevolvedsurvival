package org.dudullle.pixels.evolved.survival.utils;

import java.util.ArrayList;
import java.util.List;

public class Lag implements Runnable {
    private static Lag INSTANCE = null;
    private Long lastTickTime = System.currentTimeMillis();
    private Long lastTickDuration = 0L;
    private final List<Long> lastTickDurations = new ArrayList<>();
    private final int loadedIndex = 0;

    public Lag() {
        INSTANCE = this;
    }

    public static final double getTPS() {
        if (INSTANCE == null) {
            return 20.0D;
        } else {
            return INSTANCE.getAverageTPS();
        }
    }

    private void addValue(Long value) {
        if (lastTickDurations.size() != 20) {
            lastTickDurations.add(value);
        } else {
            lastTickDurations.remove(0);
            lastTickDurations.add(value);
        }
    }

    @Override
    public void run() {
        //Temps par tick
        this.lastTickDuration = System.currentTimeMillis() - lastTickTime;

        this.lastTickTime = System.currentTimeMillis();
        this.addValue(lastTickDuration);
    }

    public double getAverageTPS() {
        //Tick par sec
        Double mean = 0D;
        for (Long tickDuration : lastTickDurations) {
            mean += tickDuration;
        }
        Double tps = mean / 50D;
        return tps;
    }
}