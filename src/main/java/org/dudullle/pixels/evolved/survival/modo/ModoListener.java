package org.dudullle.pixels.evolved.survival.modo;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.dudullle.pixels.evolved.survival.PluginBungee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ModoListener implements Listener {
    List<String> allowedNames = new ArrayList<>();

    @EventHandler
    public void onPlayerPing(ProxyPingEvent e) {
        ServerPing sp = new ServerPing();

        sp.setDescriptionComponent(new TextComponent("&c&m&l____&r &d&lP&2&li&2&lx&3&le&4&ll&5&ls&6&lE&7&lv&8&lo&9&ll&a&lv&b&le&c&ld &m____&r\n&e&k!!!!&d Survie 1.16 en équipes ! - &bEvents &6 - Jeux &e&k!!!!".replace("&", "§")));
        ServerPing.Protocol proto = e.getResponse().getVersion();
        proto.setName("Pixels Evolved !");
        ServerPing.Players pl = new ServerPing.Players(10000, ProxyServer.getInstance().getPlayers().size(), null);
        ServerPing.PlayerInfo[] playersInfo = new ServerPing.PlayerInfo[ProxyServer.getInstance().getPlayers().size()];
        int i = 0;
        for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            playersInfo[i] = new ServerPing.PlayerInfo(p.getDisplayName(), p.getUniqueId());
            i++;
        }
        pl.setSample(playersInfo);
        sp.setPlayers(pl);
        sp.setVersion(proto);

        e.setResponse(sp);
    }

    @EventHandler
    public void onPlayerPreLogin(PreLoginEvent e) {
        //test Proxy
        String playerIp = e.getConnection().getAddress().getHostString();
        String playerName = e.getConnection().getName();

        try {
            int isUsingProxy = PluginBungee.INSTANCE.Proxy.IsProxy(playerIp);
            if (isUsingProxy > 0) {
                e.setCancelled(true);
                e.setCancelReason(new TextComponent("§cConnexion bloquée / Unable to join : PROXY"));
                for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers().stream().filter(x -> x.hasPermission("bungeecord.command.modo")).collect(Collectors.toList())) {
                    p.sendMessage(new TextComponent("§c[PROXY] : Tentative de " + playerName + " bloquée."));
                }
            } else {
                //Etape 2 : Pas de proxy, on check si ban
                ModoPlayer modoPlayer = ModoManager.INSTANCE.getPlayer(playerName);
                if (modoPlayer == null) {
                    ModoManager.INSTANCE.registerNewPlayer(null, playerName, playerIp);
                    modoPlayer = ModoManager.INSTANCE.getPlayer(playerName);
                }

                boolean banned = ModoManager.INSTANCE.isBanned(modoPlayer);
                List<ModoPlayer> otherAccounts = ModoManager.INSTANCE.findPlayersByIp(playerIp);
                List<String> names = new ArrayList<>();
                boolean isOtherAccountBanned = false;
                for (ModoPlayer otherAccount : otherAccounts) {
                    names.add(otherAccount.getName());
                    if (ModoManager.INSTANCE.isBanned(otherAccount)) {
                        isOtherAccountBanned = true;
                    }
                }
                if (banned) {
                    e.setCancelled(true);
                    e.setCancelReason(new TextComponent(ModoManager.INSTANCE.getBanMessage(modoPlayer)));
                } else if (isOtherAccountBanned) {
                    e.setCancelled(true);
                    ModoManager.INSTANCE.banPlayer(ModoBan.ModoBanReason.DOUBLE_COMPTE, "Double compte de banni", 31556952000L, playerName, null);
                    e.setCancelReason(new TextComponent(ModoManager.INSTANCE.getBanMessage(modoPlayer)));
                } else {
                    ProxyServer.getInstance().broadcast(new TextComponent("§d-> Connexion sur PE de §a" + playerName));
                    for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers().stream().filter(x -> x.hasPermission("bungeecord.command.modo")).collect(Collectors.toList())) {
                        p.sendMessage(new TextComponent("§7[Comtpes] : Comptes de " + playerName + " : " + String.join(", ", names)));
                    }
                }
            }
        } catch (IOException exception) {
            ProxyServer.getInstance().getLogger().warning("Could not check for proxy !");
            if (!allowedNames.contains(playerName)) {
                e.setCancelled(true);
                e.setCancelReason(new TextComponent("§cPetit problème de vérification anti bot... \n§dRéessaye !"));
                allowedNames.add(playerName);
            }
        }

    }

    @EventHandler
    public void onPlayerLogin(PostLoginEvent e) {
        ModoPlayer modoPlayer = ModoManager.INSTANCE.getPlayer(e.getPlayer().getName());
        if (modoPlayer != null && modoPlayer.getUuid() == null) {
            modoPlayer.setUuid(e.getPlayer().getUniqueId());
        }
        for (ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()) {
            pp.sendMessage(new TextComponent("§e->§6 " + e.getPlayer().getName() + " se connecte à §cPE."));
        }
    }

    @EventHandler
    public void onPlayerLogout(PlayerDisconnectEvent e) {
        for (ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()) {
            pp.sendMessage(new TextComponent("§e->§6 " + e.getPlayer().getName() + " se déconnecte de §cPE."));
        }
    }

    @EventHandler
    public void onPlayerChat(ChatEvent e) {
        if (!e.isCommand()) {
            ProxiedPlayer p = (ProxiedPlayer) e.getSender();
            for (ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()) {
                if (!pp.getServer().getInfo().getName().equals(p.getServer().getInfo().getName())) {
                    pp.sendMessage("§7§l[" + p.getServer().getInfo().getName() + "] §9" + p.getName() + "§7 " + e.getMessage());
                }
                for (String word : e.getMessage().split(" ")) {
                    if (pp.getName().equalsIgnoreCase(word)) {
                        Title title = ProxyServer.getInstance().createTitle();
                        title.fadeIn(10);
                        title.fadeOut(30);
                        title.stay(60);
                        title.title(new TextComponent("§eRegardez le chat !"));
                        title.subTitle(new TextComponent("§a§l" + p.getName() + " §2vous a mentionné !"));
                        pp.sendTitle(title);
                    }
                }
            }
        }
    }
}
