package org.dudullle.pixels.evolved.survival.modo;

public class ModoInsult {
    String insult;
    Long date;

    public ModoInsult(String insult, Long date) {
        this.insult = insult;
        this.date = date;
    }

    public String getInsult() {
        return insult;
    }

    public Long getDate() {
        return date;
    }
}
