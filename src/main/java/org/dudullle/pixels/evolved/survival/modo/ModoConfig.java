package org.dudullle.pixels.evolved.survival.modo;

import java.util.ArrayList;
import java.util.List;

public class ModoConfig {
    private final List<String> insultes;

    public ModoConfig() {
        this.insultes = new ArrayList<>();
    }

    public void initDefaultInsults() {
        insultes.add("con");
        insultes.add("connard");
        insultes.add("conard");
        insultes.add("conar");
        insultes.add("connar");
        insultes.add("kon");
        insultes.add("conare");
        insultes.add("konar");
        insultes.add("konnar");
        insultes.add("konnard");
        insultes.add("konard");
    }

    public final List<String> getInsultes() {
        return new ArrayList<>(insultes);
    }
}
