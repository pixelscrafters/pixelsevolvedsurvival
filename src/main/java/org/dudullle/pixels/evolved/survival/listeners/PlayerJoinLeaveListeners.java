package org.dudullle.pixels.evolved.survival.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.alliances.AllianceManager;
import org.dudullle.pixels.evolved.survival.alliances.PeAlliance;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.permissions.PermissionManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlayerJoinLeaveListeners implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(event.getPlayer().getUniqueId());
        if (pePlayer != null) {
            PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
            if (((PluginMain) PluginMain.getInstance()).isLobby()) {
                DateFormat df = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
                Date date = new Date(pePlayer.getLastLogin());
                event.setJoinMessage("§d -> Le joueur §b" + event.getPlayer().getName() + "§d s'est connecté §7Dernière co. " + df.format(date));
            } else if (alliance != null) {
                event.setJoinMessage("§d -> Le joueur §b" + event.getPlayer().getName() + "§d s'est connecté au §eRoyaume");
            }
        } else {
            Player p = event.getPlayer();
            event.setJoinMessage("§5§lSouhaitez la bienvenue à §d" + p.getPlayer().getName());
            pePlayer = new PePlayer();
            pePlayer.setUuid(p.getUniqueId());
            pePlayer.setLastLogin(0);
            pePlayer.setName(p.getName());
            PlayersManager.INSTANCE.addNewPePlayer(pePlayer);
        }
        PermissionManager.INSTANCE.setPermissionsOfPlayer(event.getPlayer());
        //Téléportation au spawn
        event.getPlayer().teleportAsync(event.getPlayer().getWorld().getSpawnLocation());
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(event.getPlayer().getUniqueId());
        if (!((PluginMain) PluginMain.getInstance()).isLobby()) {
            if (pePlayer != null) {
                PeAlliance alliance = AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer);
                if (alliance == null) {
                    event.setKickMessage("§4Il te faut une alliance pour jouer au Royaume. Si tu viens juste d'en créer une, §eattends max 2 minutes §4le temps de synchroniser tout ça :)");
                    event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
                }
            } else {
                event.setKickMessage("§4Bienvenue nouveau joueur. Il faut 2 minutes maximum avant de synchroniser tes données sur tout le serveur... Si cela dure plus de 2 min, Contacte le staff.");
                event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(event.getPlayer().getUniqueId());
        if (pePlayer != null) {
            pePlayer.setLastLogin(System.currentTimeMillis());
        }
        event.setQuitMessage("§d -> Le joueur §b" + pePlayer.getName() + " §d s'est déconnecté.");
        PermissionManager.INSTANCE.removeAttachment(event.getPlayer());
    }
}
