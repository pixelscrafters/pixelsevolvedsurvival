package org.dudullle.pixels.evolved.survival.utils;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

public class DrawUtils {
    public static final DrawUtils INSTANCE = new DrawUtils();

    private DrawUtils() {

    }

    public void drawChunk(Particle particle, Chunk chunk, Player p) {
        //World location conversion
        int minX = chunk.getX() * 16;
        int minZ = chunk.getZ() * 16;
        int maxX = chunk.getX() * 16 + 15;
        int maxZ = chunk.getZ() * 16 + 15;
        for (int x = minX; x <= maxX; x++) {
            for (int z = minZ; z <= maxZ; z++) {
                p.spawnParticle(particle, new Location(p.getWorld(), x, p.getWorld().getHighestBlockYAt(x, z) + 1, z), 1);
            }
        }
    }
}
