package org.dudullle.pixels.evolved.survival.modo;

import java.util.UUID;

public class ModoBan {
    private final ModoBanReason banReason;
    private long banExpiration;
    private final long banDate;
    private final UUID banBy;
    private final String banDescription;
    public ModoBan(ModoBanReason banReason, long banExpiration, long banDate, UUID banBy, String banDescription) {
        this.banReason = banReason;
        this.banExpiration = banExpiration;
        this.banDate = banDate;
        this.banBy = banBy;
        this.banDescription = banDescription;
    }

    public ModoBanReason getBanReason() {
        return banReason;
    }

    public long getBanExpiration() {
        return banExpiration;
    }

    public long getBanDate() {
        return banDate;
    }

    public UUID getBanBy() {
        return banBy;
    }

    public String getBanDescription() {
        return banDescription;
    }

    public void setBanExpirationOver() {
        this.banExpiration = System.currentTimeMillis() - 1;
    }

    public enum ModoBanReason {
        XRAY("Xray"),
        CHEAT("Cheat / triche"),
        INSULT("Insultes"),
        RESPECT("Manque de respect"),
        USEBUG("Usebug"),
        AFK_MACHINE("Machine AFK"),
        DOUBLE_COMPTE("Multiples comptes"),
        RULES("Ne respecte pas les règles");
        public String title;

        ModoBanReason(String title) {
            this.title = title;
        }
    }
}
