package org.dudullle.pixels.evolved.survival.modo;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.time.Duration;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ModoCommand extends Command {

    private static final Pattern periodPattern = Pattern.compile("([0-9]+)([smhdMy])");

    public ModoCommand() {
        super("modo");
    }

    public static Long parseDateDiff(String period) {
        if (period == null) return null;
        period = period.toLowerCase(Locale.FRANCE);
        Matcher matcher = periodPattern.matcher(period);
        Long duration = 0L;
        while (matcher.find()) {
            int num = Integer.parseInt(matcher.group(1));
            String typ = matcher.group(2);
            switch (typ) {
                case "s":
                    duration += duration + Duration.ofSeconds(num).toMillis();
                    break;
                case "m":
                    duration += duration + Duration.ofMinutes(num).toMillis();
                    break;
                case "h":
                    duration += duration + Duration.ofHours(num).toMillis();
                    break;
                case "d":
                    duration += duration + Duration.ofDays(num).toMillis();
                    break;
                case "M":
                    duration += duration + Duration.ofDays(num * 30).toMillis();
                    break;
            }
        }
        return duration;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission("bungeecord.command.modo")) {
            if (args.length == 0) {
                sender.sendMessage(new TextComponent("§a->/modo b PLAYER REASON DURATION DESCRIPTION"));
                sender.sendMessage(new TextComponent("§a->/modo k PLAYER REASON DESCRIPTION"));
                sender.sendMessage(new TextComponent("§a->/modo ub PLAYER"));
            } else if (args[0].equalsIgnoreCase("b")) {
                if (args.length > 4) {
                    //Commande OK
                    String playerName = args[1];
                    String reason = args[2];
                    String duration = args[3];
                    String description = "";
                    for (int i = 4; i < args.length; i++) {
                        description += args[i] + " ";
                    }
                    this.handleModoBan(sender, playerName, duration, reason, description);
                } else {
                    sender.sendMessage(new TextComponent("§cIl manque des trucs /modo pour l'aide"));
                }
            } else if (args[0].equalsIgnoreCase("k")) {

            } else if (args[0].equalsIgnoreCase("ub")) {
                if (args.length > 1 && sender instanceof ProxiedPlayer) {
                    String playerName = args[1];
                    ModoManager.INSTANCE.unban(playerName, (ProxiedPlayer) sender);
                    sender.sendMessage(new TextComponent("§aLe joueur a été unban !"));
                }
            } else if (args[0].equalsIgnoreCase("br")) {
                for (ModoBan.ModoBanReason banReason : ModoBan.ModoBanReason.values()) {
                    sender.sendMessage(new TextComponent(banReason.name()));
                }
            } else {
                sender.sendMessage(new TextComponent("§cCommande introuvable. /modo pour l'aide"));
            }

        } else {
            sender.sendMessage(new TextComponent("§cAH NON ! Permission refusée."));
        }
    }

    public void handleModoBan(CommandSender admin, String playerToBan, String duration, String reason, String description) {
        try {
            ModoBan.ModoBanReason modoBanReason = ModoBan.ModoBanReason.valueOf(reason.toUpperCase());
            long durationL = parseDateDiff(duration);
            admin.sendMessage(new TextComponent("BAN TIME MS : " + durationL));
            ModoPlayer modoPlayer = ModoManager.INSTANCE.getPlayer(playerToBan);
            if (modoPlayer != null && admin instanceof ProxiedPlayer) {
                ProxiedPlayer sender = (ProxiedPlayer) admin;
                ModoManager.INSTANCE.banPlayer(modoBanReason, description, durationL, modoPlayer.getName(), sender);
            }
        } catch (IllegalArgumentException ex) {
            admin.sendMessage(new TextComponent("§§cRaison non trouvée : /modo br"));
        }
    }
}
