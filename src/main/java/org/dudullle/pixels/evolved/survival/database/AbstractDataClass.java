package org.dudullle.pixels.evolved.survival.database;

public abstract class AbstractDataClass {
    public boolean hasBeenModified;
    public boolean hasBeenCreated;
}
