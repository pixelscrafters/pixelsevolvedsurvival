package org.dudullle.pixels.evolved.survival;

import net.md_5.bungee.api.plugin.Plugin;
import org.dudullle.pixels.evolved.survival.modo.IP2Proxy;
import org.dudullle.pixels.evolved.survival.modo.ModoCommand;
import org.dudullle.pixels.evolved.survival.modo.ModoListener;
import org.dudullle.pixels.evolved.survival.modo.ModoManager;

import java.io.IOException;
import java.util.logging.Level;

public class PluginBungee extends Plugin {
    public static PluginBungee INSTANCE;
    public IP2Proxy Proxy = new IP2Proxy();


    @Override
    public void onEnable() {
        super.onEnable();
        INSTANCE = this;

        try {
            if (Proxy.Open("IP2PROXY-LITE-PX3.BIN", IP2Proxy.IOModes.IP2PROXY_MEMORY_MAPPED) == 0) {
                getLogger().log(Level.FINE, "ProxyDB -> Ok");
            }
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "ProxyDB -> NON EFFECTIVE !!");
        }
        ModoManager.INSTANCE.loadService();
        getProxy().getPluginManager().registerListener(this, new ModoListener());

        getProxy().getPluginManager().registerCommand(this, new ModoCommand());
    }

    @Override
    public void onDisable() {
        super.onDisable();
        ModoManager.INSTANCE.shutdownService();
    }
}
