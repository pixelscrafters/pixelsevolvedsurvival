package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetspawnCommand extends AbstractCommand {
    public static final String COMMAND_NAME = "setspawn";
    public static final SetspawnCommand INSTANCE = new SetspawnCommand();

    private SetspawnCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Location spawn = player.getLocation();
            player.getWorld().setSpawnLocation(spawn);
        } else {
            sender.sendMessage("Merci de faire cette commande en jeu");
        }
        return true;
    }
}
