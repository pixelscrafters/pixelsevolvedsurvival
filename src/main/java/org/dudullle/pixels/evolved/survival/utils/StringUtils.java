package org.dudullle.pixels.evolved.survival.utils;

public class StringUtils {
    public static String convertForSB(String name, String value) {
        String s = "§d§l" + name + "§b : " + value;
        if (value.equals("")) {
            s = "§d" + name;
        }

        if (s.length() > 39) {
            return s.substring(0, 39);
        }
        return s;
    }

    public static String getLagColor(double d) {
        if (d < 2f) {
            return "§2";
        } else if (d < 4f) {
            return "§a";
        } else if (d < 6f) {
            return "§6";
        } else if (d < 10f) {
            return "§c";
        } else if (d < 30f) {
            return "§4";
        } else {
            return "§4OULA : ";
        }
    }

    public static String getRegionNameFromAllianceAndCoords(int chunkX, int chunkZ, String allianceName) {
        return allianceName.replace("[", "").replace("]", "").replace(" ", "").trim() + "__" + chunkX + "__" + chunkZ;
    }
}
