package org.dudullle.pixels.evolved.survival.apirest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;

public class APIReturnObject {
    private final ReturnMessage returnCode;
    private final Object data;

    public APIReturnObject(ReturnMessage returnCode, Object data) {
        this.returnCode = returnCode;
        this.data = data;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
