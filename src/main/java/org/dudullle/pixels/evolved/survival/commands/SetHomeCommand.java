package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;

public class SetHomeCommand extends AbstractCommand {
    public static final SetHomeCommand INSTANCE = new SetHomeCommand();
    public static final String COMMAND_NAME = "sethome";

    private SetHomeCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (((PluginMain) PluginMain.getInstance()).isLobby()) {
                p.sendMessage("§cVous ne pouvez pas claim au lobby !");
            } else {
                PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
                pePlayer.setHome(new AlliancePointOfInterest(p.getLocation()));
                p.sendMessage("§aNouveau home paramétré !");
            }
        } else {
            sender.sendMessage("§cLa console ne peut pas set de home");
        }
        return true;
    }
}
