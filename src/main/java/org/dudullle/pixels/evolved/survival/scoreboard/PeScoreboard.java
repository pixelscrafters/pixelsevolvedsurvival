package org.dudullle.pixels.evolved.survival.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.dudullle.pixels.evolved.survival.utils.StringUtils;

import java.util.TreeMap;

public class PeScoreboard {
    TreeMap<String, Integer> mapEntries = new TreeMap<>();
    private final Scoreboard scoreboard;
    private final Objective obj;
    private int id = 20;

    public PeScoreboard(Player player) {
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        obj = scoreboard.registerNewObjective("PE", "dummy", "§d---§5P-E Survie§d---");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void addEntry(String entryId, String title, String value) {
        Integer colorId = mapEntries.get(entryId);
        if (colorId == null) {
            mapEntries.put(entryId, id);
            colorId = id;
            id--;
        }
        String color = ChatColor.values()[colorId].toString();
        Team entryTeam = scoreboard.registerNewTeam(entryId);
        entryTeam.addEntry(color + "" + color);
        entryTeam.setPrefix(StringUtils.convertForSB(title, value));
        entryTeam.setDisplayName("test");
        obj.getScore(color + "" + color).setScore(colorId);
    }

    public void setEntry(String entryId, String title, String value) {
        if (!mapEntries.containsKey(entryId)) {
            //Team entryTeam = scoreboard.getEntryTeam(entryId);
            addEntry(entryId, title, value);
        }
    }

    public void setScoreboard(Player p) {
        p.setScoreboard(scoreboard);
    }
}
