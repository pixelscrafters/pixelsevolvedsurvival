package org.dudullle.pixels.evolved.survival.modo;

import java.util.UUID;

public class ModoKick {
    private final ModoKickReason kickReason;
    private final UUID kickBy;
    private final String kickDescription;
    private final long kickDate;

    public ModoKick(ModoKickReason kickReason, UUID kickBy, String kickDescription, long kickDate) {
        this.kickReason = kickReason;
        this.kickBy = kickBy;
        this.kickDescription = kickDescription;
        this.kickDate = kickDate;
    }

    public ModoKickReason getKickReason() {
        return kickReason;
    }

    public UUID getKickBy() {
        return kickBy;
    }

    public String getKickDescription() {
        return kickDescription;
    }

    public long getKickDate() {
        return kickDate;
    }
    public enum ModoKickReason {
        INSULT("Insulte"),
        DUAL_ACCOUNT("Double compte"),
        SPAM("Spam"),
        AFK("AFK"),
        SYSTEM("Kick par le système - Reconnectez vous.");
        public String title;

        ModoKickReason(String title) {
            this.title = title;
        }
    }
}
