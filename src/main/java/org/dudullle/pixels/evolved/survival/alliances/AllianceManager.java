package org.dudullle.pixels.evolved.survival.alliances;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;
import org.dudullle.pixels.evolved.survival.utils.StringUtils;

import java.util.*;

public class AllianceManager {
    public static final AllianceManager INSTANCE = new AllianceManager();
    public static final String DEFAULT_ALLIANCE_NAME = "[%ID%] Alliance de %NAME%";
    static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static final int CHARS_LENGTH = CHARS.length();
    protected List<PeAlliance> alliances = new ArrayList<>();

    public PeAlliance getAllianceByPePlayer(PePlayer pePlayer) {
        Optional<PeAlliance> alliance = alliances.stream().filter(x -> x.isMember(pePlayer)).findFirst();
        return alliance.orElse(null);
    }

    public ReturnMessage createAlliance(PePlayer pePlayer) {
        if (getAllianceByPePlayer(pePlayer) != null) {
            return ReturnMessage.ERROR_ALREADY_MEMBER;
        } else {
            Random rnd = new Random(System.currentTimeMillis());
            alliances.add(new PeAlliance(pePlayer, DEFAULT_ALLIANCE_NAME.replace("%NAME%", pePlayer.getName()).replace("%ID%", convert(rnd.nextInt()))));
            return ReturnMessage.SUCCESS;
        }
    }

    public String convert(int id) {
        StringBuilder sb = new StringBuilder();
        do {
            sb.append(CHARS.charAt(id % CHARS_LENGTH));
            id = id / CHARS_LENGTH;
        } while (id != 0);
        return sb.toString();
    }

    public ReturnMessage removeAlliance(PePlayer pePlayer) {
        PeAlliance alliance = getAllianceByPePlayer(pePlayer);
        if (alliance == null) {
            return ReturnMessage.ERROR_NOT_IN_ALLIANCE;
        } else if (!alliance.isAdmin(pePlayer)) {
            return ReturnMessage.ERROR_NOT_ADMIN;
        } else {
            alliances.remove(alliance);
            return ReturnMessage.SUCCESS;
        }
    }

    protected void removeAlliance(PeAlliance alliance) {
        this.alliances.remove(alliance);
    }

    public List<PeAlliance> getAlliances() {
        return new ArrayList<>(alliances);
    }

    public PeAlliance getByName(String name) {
        Optional<PeAlliance> alliance = alliances.stream().filter(x -> x.getName().equals(name)).findFirst();
        return alliance.orElse(null);
    }

    public AbstractMap.SimpleEntry<PeAlliance, PeAllianceFlag> getFlagByCoordinates(int x, int z) {
        Optional<PeAlliance> allianceOptional = alliances.stream().filter(al -> al.getFlagAt(x, z) != null).findFirst();
        if (allianceOptional.isPresent()) {
            PeAlliance alliance = allianceOptional.get();
            return new AbstractMap.SimpleEntry<>(alliance, alliance.getFlagAt(x, z));
        }
        return null;
    }

    /**
     * Méthode qui permet de créer un claim via worldguard.
     *
     * @param alliance
     * @param chunkX
     * @param chunkZ
     * @param allianceFlag
     * @param world
     */
    public ReturnMessage createWorldGuardRegion(PeAlliance alliance, int chunkX, int chunkZ, PeAllianceFlag allianceFlag, World world) {
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionManager regions = container.get(WorldGuard.getInstance().getPlatform().getMatcher().getWorldByName(world.getName()));
        String regionName = StringUtils.getRegionNameFromAllianceAndCoords(chunkX, chunkZ, alliance.getName());
        BlockVector3 minPos = BlockVector3.at(chunkX * 16, 0, chunkZ * 16);
        BlockVector3 maxPos = BlockVector3.at(chunkX * 16 + 15, 256, chunkZ * 16 + 15);
        ProtectedCuboidRegion protectedCuboidRegion = new ProtectedCuboidRegion(regionName, minPos, maxPos);
        alliance.getMembers().forEach(x -> protectedCuboidRegion.getMembers().addPlayer(x));
        alliance.getAdmins().forEach(x -> protectedCuboidRegion.getOwners().addPlayer(x));
        regions.addRegion(protectedCuboidRegion);
        return ReturnMessage.SUCCESS;
    }

    /**
     * Méthode qui réinitialise les membres d'une région.
     *
     * @param alliance
     * @param chunkX
     * @param chunkZ
     * @param allianceFlag
     * @param world
     */
    public ReturnMessage updatePlayersRegion(PeAlliance alliance, int chunkX, int chunkZ, PeAllianceFlag allianceFlag, World world) {
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionManager regions = container.get(WorldGuard.getInstance().getPlatform().getMatcher().getWorldByName(world.getName()));
        String regionName = StringUtils.getRegionNameFromAllianceAndCoords(chunkX, chunkZ, alliance.getName());
        try {
            ProtectedCuboidRegion protectedCuboidRegion = (ProtectedCuboidRegion) regions.getRegion(regionName);
            protectedCuboidRegion.getMembers().clear();
            protectedCuboidRegion.getOwners().clear();
            alliance.getMembers().forEach(x -> protectedCuboidRegion.getMembers().addPlayer(x));
            alliance.getAdmins().forEach(x -> protectedCuboidRegion.getOwners().addPlayer(x));
            return ReturnMessage.SUCCESS;
        } catch (Exception ex) {
            Bukkit.getLogger().severe("§4[§dPE SURVIVAL§4] ERROR WHILE UPDATING REGION " + regionName);
            Bukkit.getLogger().severe(ex.getLocalizedMessage() + "\n" + ex.getStackTrace());
            return ReturnMessage.ERROR_UPDATING_REGION;
        }
    }

    public ReturnMessage removeWorldguardRegion(PeAlliance alliance, int chunkX, int chunkZ, PeAllianceFlag allianceFlag, World world) {
        if (world == null) {
            return ReturnMessage.ERROR_NOT_EXISTS;
        }
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionManager regions = container.get(WorldGuard.getInstance().getPlatform().getMatcher().getWorldByName(world.getName()));
        String regionName = StringUtils.getRegionNameFromAllianceAndCoords(chunkX, chunkZ, alliance.getName());
        regions.removeRegion(regionName);
        return ReturnMessage.SUCCESS;
    }
}
