package org.dudullle.pixels.evolved.survival;

import net.milkbowl.vault.economy.Economy;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.TomcatURLStreamHandlerFactory;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.dudullle.pixels.evolved.survival.alliances.AllianceDatabaseManager;
import org.dudullle.pixels.evolved.survival.alliances.AllianceManager;
import org.dudullle.pixels.evolved.survival.apirest.APIPePlayer;
import org.dudullle.pixels.evolved.survival.apirest.APItest;
import org.dudullle.pixels.evolved.survival.commands.*;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.database.WarpManager;
import org.dudullle.pixels.evolved.survival.listeners.InventoryListener;
import org.dudullle.pixels.evolved.survival.listeners.PlayerJoinLeaveListeners;
import org.dudullle.pixels.evolved.survival.listeners.PlayerListener;
import org.dudullle.pixels.evolved.survival.listeners.customevents.EndOfTimeEvent;
import org.dudullle.pixels.evolved.survival.money.MoneyManager;
import org.dudullle.pixels.evolved.survival.permissions.PermissionManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.utils.Lag;

import java.io.File;
import java.util.logging.Logger;

public class PluginMain extends JavaPlugin {
    public static final Logger LOG = Bukkit.getLogger();
    private static JavaPlugin instance;
    private Tomcat tomcat;

    public static JavaPlugin getInstance() {
        return instance;
    }

    public Boolean isLobby() {
        return Bukkit.getWorld("world") != null;
    }

    private void registerCommands() {
        this.getCommand(MoneyCommand.COMMAND_NAME).setExecutor(new MoneyCommand());
        this.getCommand(AllianceCommand.COMMAND_NAME).setExecutor(AllianceCommand.INSTANCE);
        this.getCommand(GiveFlagCommand.COMMAND_NAME).setExecutor(GiveFlagCommand.INSTANCE);
        this.getCommand(PermissionCommand.COMMAND_NAME).setExecutor(PermissionCommand.INSTANCE);
        this.getCommand(GamemodeCommand.COMMAND_NAME).setExecutor(GamemodeCommand.INSTANCE);
        this.getCommand(HomeCommand.COMMAND_NAME).setExecutor(HomeCommand.INSTANCE);
        this.getCommand(SetHomeCommand.COMMAND_NAME).setExecutor(SetHomeCommand.INSTANCE);
        this.getCommand(TpnCommand.COMMAND_NAME).setExecutor(TpnCommand.INSTANCE);
        this.getCommand(TpyCommand.COMMAND_NAME).setExecutor(TpyCommand.INSTANCE);
        this.getCommand(TpaCommand.COMMAND_NAME).setExecutor(TpaCommand.INSTANCE);
        this.getCommand(VanishCommand.COMMAND_NAME).setExecutor(VanishCommand.INSTANCE);
        this.getCommand(SpawnCommand.COMMAND_NAME).setExecutor(SpawnCommand.INSTANCE);
        this.getCommand(SetspawnCommand.COMMAND_NAME).setExecutor(SetspawnCommand.INSTANCE);
        this.getCommand(RandomTeleportCommand.COMMAND_NAME).setExecutor(RandomTeleportCommand.INSTANCE);
        this.getCommand(WarpCommand.COMMAND_NAME).setExecutor(WarpCommand.INSTANCE);
    }

    private void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerJoinLeaveListeners(), this);
        getServer().getPluginManager().registerEvents(new InventoryListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
    }

    @Override
    public void onEnable() {
        super.onEnable();
        if (isLobby()) {
            tomcatServer();
        }
        instance = this;
        LOG.info("§aDémarrage du plugin Pixels Evolved Survival.");

        LOG.info("§aConnexion et chargement des données");
        PlayersManager.INSTANCE.loadService();
        AllianceDatabaseManager.INSTANCE.loadService();
        PermissionManager.INSTANCE.loadService();
        WarpManager.INSTANCE.readAll();

        registerCommands();
        registerListeners();

        if (!setupEconomy()) {
            LOG.severe("Impossible de démarrer l'économie.");
        }

        startAutoSave();
        startAutoSavePermissions();
        startSBSync();
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Lag(), 0L, 1L);
    }

    public void tomcatServer() {
        tomcat = new Tomcat();
        tomcat.setPort(8382);
        Connector connector = tomcat.getConnector();

        connector.setURIEncoding("UTF-8");

        File docBase = new File(System.getProperty("java.io.tmpdir"));
        Context ctxt = tomcat.addContext("", docBase.getAbsolutePath());

        Tomcat.addServlet(ctxt, "PeAPITest", new APItest());
        Tomcat.addServlet(ctxt, "PeAPIPlayers", new APIPePlayer());
        ctxt.addServletMappingDecoded("/test", "PeAPITest");
        ctxt.addServletMappingDecoded("/players", "PeAPIPlayers");
        TomcatURLStreamHandlerFactory.disable();

        try {
            tomcat.start();
        } catch (LifecycleException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
        try {
            tomcat.stop();
            tomcat.destroy();
        } catch (LifecycleException e) {
            e.printStackTrace();
        }
        PlayersManager.INSTANCE.refreshToDatabase();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            LOG.severe("Vault introuvable !");
            return false;
        } else {
            Bukkit.getServicesManager().register(Economy.class, MoneyManager.INSTANCE, getServer().getPluginManager().getPlugin("Vault"), ServicePriority.Low);
            Bukkit.getPluginManager().getPlugin("Vault").getLogger().info("§aAjout du plugin d'économie avec succès");
        }
        return true;
    }

    private void startAutoSave() {
        //Toutes les 5 min
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                Bukkit.getLogger().info("Refreshing data ...");
                //KEEP¨THIS ORDER
                WarpManager.INSTANCE.delete();
                WarpManager.INSTANCE.write();


                PlayersManager.INSTANCE.refreshToDatabase();
                try {
                    AllianceDatabaseManager.INSTANCE.refreshFromDatabase();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                long endTime = System.currentTimeMillis();
                Bukkit.getLogger().info("Done in " + (endTime - startTime) + "ms");
            }
        }, 20L * 5, 20L * 60);
    }

    private void startAutoSavePermissions() {
        //Toutes les 5 min
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                PlayersManager.INSTANCE.refreshToDatabase();
                try {
                    PermissionManager.INSTANCE.refresh();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, 20L * 5, 20L * 60); //TODO mettre à 30 min en prod.
    }

    private void startSBSync() {
        Bukkit.getScheduler().runTaskTimer(this, new Runnable() {
            @Override
            public void run() {

                for (Player p : Bukkit.getOnlinePlayers()) {
                    PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
                    if (pePlayer != null) {
                        pePlayer.syncScoreboard(p);
                        if (pePlayer.isUsingClaimItem() && !isLobby()) {
                            EndOfTimeEvent event = new EndOfTimeEvent(EndOfTimeEvent.ActionType.DO_CLAIM_TICK, pePlayer, AllianceManager.INSTANCE.getAllianceByPePlayer(pePlayer), p);
                            // Call the event
                            Bukkit.getServer().getPluginManager().callEvent(event);
                        }
                    }
                }
            }
        }, 10L, 10L);
    }
}
