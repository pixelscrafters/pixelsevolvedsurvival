package org.dudullle.pixels.evolved.survival.alliances;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.Material;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class PeAllianceFlag {
    private static final Map<FlagType, Integer> MAX_HEALTH = new TreeMap<FlagType, Integer>() {{
        put(FlagType.MASTER, 1000);
        put(FlagType.AGRICULTURE, 50);
        put(FlagType.MINING, 50);
        put(FlagType.NORMAL, 100);
        put(FlagType.REINFORCED, 1000);
        put(FlagType.UNCLAIM, -10000);
    }};

    private static final Map<FlagType, Integer> REMOVAL_COST = new TreeMap<FlagType, Integer>() {{
        put(FlagType.MASTER, 100000);
        put(FlagType.AGRICULTURE, 5000);
        put(FlagType.MINING, 5000);
        put(FlagType.NORMAL, 10000);
        put(FlagType.REINFORCED, 25000);
        put(FlagType.UNCLAIM, 0);
    }};
    /**
     * Liste des joueurs autorisés à modifier les blocs dans la zone
     */
    private final ArrayList<PePlayer> allowedModification = new ArrayList<>();
    /**
     * Emplacement du drapeau X
     */
    private final int xLocation;
    /**
     * Emplacement du drapeau Z
     */
    private final int zLocation;
    /**
     * Nom du monde.
     */
    private final String worldName;
    /**
     * Vie du drapeau
     */
    private int health;
    /**
     * Type du drapeau
     */
    private final FlagType flagType;

    public PeAllianceFlag(int xLocation, int zLocation, String worldName, FlagType flagType) {
        this.xLocation = xLocation;
        this.zLocation = zLocation;
        this.worldName = worldName;
        this.flagType = flagType;
    }

    public static PeAllianceFlag fromJson(String json) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, PeAllianceFlag.class);
    }

    public static Map<FlagType, Integer> getMaxHealth() {
        return MAX_HEALTH;
    }

    public static Integer getMaxHealth(FlagType flagType) {
        if (MAX_HEALTH.containsKey(flagType)) {
            return MAX_HEALTH.get(flagType);
        } else {
            return 0;
        }
    }
    /**
     * Liste des items récupérés automatiquement par le drapeau (dépend du type)
     */
    //private final Map<ItemStack, Integer> itemsInsideFlag = new TreeMap<>();

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    public String getWorldName() {
        return worldName;
    }

    public int getxLocation() {
        return xLocation;
    }

    public int getzLocation() {
        return zLocation;
    }

    public int getHealth() {
        return health;
    }

    public FlagType getFlagType() {
        return flagType;
    }

    public ArrayList<PePlayer> getAllowedModification() {
        return allowedModification;
    }

    /*public Map<ItemStack, Integer> getItemsInsideFlag() {
        return itemsInsideFlag;
    }*/

    public int getRemovalCosts() {
        return REMOVAL_COST.get(flagType);
    }


    public enum FlagType {
        MASTER {
            @Override
            public Material getMaterial() {
                return Material.BEDROCK;
            }

            @Override
            public String getTranslatedName() {
                return "Territoire principal";
            }

            @Override
            public String getDescription() {
                return "Claim central d'alliance. Si détruit, les territoires dépendants le sont aussi";
            }
        },
        AGRICULTURE {
            @Override
            public Material getMaterial() {
                return Material.COARSE_DIRT;
            }

            @Override
            public String getTranslatedName() {
                return "Territoire agricole";
            }

            @Override
            public String getDescription() {
                return "Claim rapportant des ressources agricoles";
            }
        },
        MINING {
            @Override
            public Material getMaterial() {
                return Material.STONE_BRICK_WALL;
            }

            @Override
            public String getTranslatedName() {
                return "Territoire minier";
            }

            @Override
            public String getDescription() {
                return "Claim rapportant des ressources minières";
            }
        },
        NORMAL {
            @Override
            public Material getMaterial() {
                return Material.CHEST;
            }

            @Override
            public String getTranslatedName() {
                return "Territoire d'alliance libre";
            }

            @Override
            public String getDescription() {
                return "Claim destiné à un usage de build";
            }
        },
        REINFORCED {
            @Override
            public Material getMaterial() {
                return Material.OBSIDIAN;
            }

            @Override
            public String getTranslatedName() {
                return "Territoire d'alliance renforcé libre";
            }

            @Override
            public String getDescription() {
                return "Claim renforcé face aux attaques. Usage de build.";
            }
        },
        UNCLAIM {
            @Override
            public Material getMaterial() {
                return Material.BARRIER;
            }

            @Override
            public String getTranslatedName() {
                return "§4Supprimer un claim";
            }

            @Override
            public String getDescription() {
                return "Cliquez pour supprimer un claim !";
            }
        };

        public abstract Material getMaterial();

        public abstract String getTranslatedName();

        public abstract String getDescription();
    }
}
