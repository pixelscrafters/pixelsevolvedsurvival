package org.dudullle.pixels.evolved.survival.players;

public enum PePlayerDataType {
    HOME,
    BAN,
    BAN_TIME,
    KICK,
}
