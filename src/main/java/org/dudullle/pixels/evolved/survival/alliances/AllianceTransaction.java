package org.dudullle.pixels.evolved.survival.alliances;

public class AllianceTransaction {
    private final String allianceName;
    private final DataType dataType;
    private final DataOperation dataOperation;
    private final String value;
    private long id;
    private long saveDate;

    public AllianceTransaction(long id, String allianceName, long saveDate, DataType dataType, DataOperation dataOperation, String value) {
        this.id = id;
        this.allianceName = allianceName;
        this.saveDate = saveDate;
        this.dataType = dataType;
        this.dataOperation = dataOperation;
        this.value = value;
    }

    public AllianceTransaction(String allianceName, long saveDate, DataType dataType, DataOperation dataOperation, String value) {
        this.allianceName = allianceName;
        this.saveDate = saveDate;
        this.dataType = dataType;
        this.dataOperation = dataOperation;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public String getAllianceName() {
        return allianceName;
    }

    public long getSaveDate() {
        return saveDate;
    }

    public void setSaveDate(long saveDate) {
        this.saveDate = saveDate;
    }

    public DataType getDataType() {
        return dataType;
    }

    public DataOperation getDataOperation() {
        return dataOperation;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "AllianceTransaction{" +
                "id=" + id +
                ", allianceName='" + allianceName + '\'' +
                ", saveDate=" + saveDate +
                ", dataType=" + dataType +
                ", dataOperation=" + dataOperation +
                ", value='" + value + '\'' +
                '}';
    }

    public enum DataType {
        FLAG,
        ADMIN,
        MEMBER,
        WAITING_MEMBER,
        HOME,
        ALL
    }

    public enum DataOperation {
        ADD,    /*ADD*/
        RMV,    /*REMOVE*/
        DST,    /*DESTROY*/
        SET,    /*SET*/
    }
}
