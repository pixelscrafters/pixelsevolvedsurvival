package org.dudullle.pixels.evolved.survival.database;

import org.bukkit.Bukkit;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.money.Transaction;
import org.dudullle.pixels.evolved.survival.players.PePlayer;
import org.dudullle.pixels.evolved.survival.players.PePlayerData;
import org.dudullle.pixels.evolved.survival.players.PePlayerDataType;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class PlayersManager {
    public static final PlayersManager INSTANCE = new PlayersManager();

    private final List<PePlayer> players = new ArrayList<>();
    private long lastLoadSavePePlayerData = 0;

    /**
     * Méthode utilisée pour synchroniser les données avec la BDD.
     */
    public void refreshFromDatabase() {
        //ASYNC !!!!
    }

    public void refreshToDatabase() {
        Object[] copyOfPlayers = Arrays.copyOf(players.toArray(), players.size());
        Bukkit.getScheduler().runTaskAsynchronously(PluginMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                Connection connection = null;
                try {
                    connection = DbManager.INSTANCE.openConnection();
                    for (Object objPePlayer : copyOfPlayers) {
                        PePlayer pePlayer = (PePlayer) objPePlayer;
                        Bukkit.getLogger().info("Sauvegarde du joueur " + pePlayer);
                        saveOrUpdatePlayerSlow(pePlayer, connection);
                    }
                    loadAllPePlayerData(connection);
                } catch (SQLException throwables) {
                    Bukkit.getLogger().severe("Erreur de SQL au démarrage. " + throwables.getLocalizedMessage());
                } finally {
                    if (connection != null) {
                        try {
                            DbManager.INSTANCE.closeConnection(connection);
                        } catch (SQLException throwables) {
                            Bukkit.getLogger().severe("Erreur de déconnexion SQL au démarrage.");
                        }
                    }
                }
            }
        });
    }

    public List<PePlayer> getPlayers() {
        return new ArrayList<>(players);
    }

    public void saveOrUpdatePlayerSlow(PePlayer pePlayer, Connection conn) throws SQLException {
        //On sauvegarde le joueur
        if (pePlayer.hasBeenCreated) {
            pePlayer.hasBeenCreated = false;
            pePlayer.hasBeenModified = false;

            PreparedStatement stm = conn.prepareStatement("INSERT INTO `players` (`id`, `uuid`, `name`, `lastonline`) VALUES (NULL, ?, ?, ?);");
            stm.setString(1, pePlayer.getUuid().toString());
            stm.setString(2, pePlayer.getName());
            stm.setLong(3, pePlayer.getLastLogin());
            stm.executeUpdate();
        } else if (pePlayer.hasBeenModified) {
            pePlayer.hasBeenModified = false;

            PreparedStatement stm = conn.prepareStatement("UPDATE `players` SET `uuid` = ?, `name` = ?, `lastonline` = ? WHERE `players`.`id` = ?; ");
            stm.setString(1, pePlayer.getUuid().toString());
            stm.setString(2, pePlayer.getName());
            stm.setLong(3, pePlayer.getLastLogin());
            stm.setLong(4, pePlayer.getId());
            stm.executeUpdate();
        } else {
            //Pas besoin de sauvegarder.
        }

        //Récupération des données depuis la dernière sauvegarde.
        //Mise à jour des transactions (DOWNLOAD)
        if (pePlayer.getTransactions() != null) {
            loadAllTransactions(conn, pePlayer, pePlayer.getLastTransactionSave());
        }
        //Sauvegarde de l'uuid
        if (pePlayer.getTransactions() != null) {
            for (Transaction tr : pePlayer.getTransactions()) {
                if (!tr.hasBeenSaved) {
                    //On sauvegarde via le DbManager
                    //TODO SAVE
                    //Si la transaction n'a pas été sauvegardée, on enregistre.
                    //On met a jour la date de sauvegarde en base de la transaction.
                    tr.setSave(System.currentTimeMillis());
                    PreparedStatement stm = conn.prepareStatement("INSERT INTO `transactions` (`id`, `uuid_from`, `uuid_to`, `amount`, `save`) VALUES (NULL, ?, ?, ?, ?);");
                    stm.setString(1, tr.getUuidFrom());
                    stm.setString(2, tr.getUuidTo());
                    stm.setDouble(3, tr.getAmount());
                    stm.setLong(4, tr.getSave());
                    stm.executeUpdate();
                    tr.hasBeenSaved = true;
                }
            }
        }

    }

    public void loadAllPlayers(Connection conn) throws SQLException {
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery("SELECT * FROM players");
        while (rs.next()) {
            PePlayer pePlayer = new PePlayer();
            pePlayer.setId(rs.getLong("id"));
            pePlayer.setLastLogin(rs.getLong("lastonline"));
            pePlayer.setUuid(UUID.fromString(rs.getString("uuid")));
            pePlayer.setName(rs.getString("name"));
            pePlayer.hasBeenModified = false;
            pePlayer.hasBeenCreated = false;
            players.add(pePlayer);
            loadAllTransactions(conn, pePlayer, 0l);
        }
        Bukkit.getLogger().info("§a" + players.size() + " ont été chargés depuis la base.");
    }

    public void loadAllTransactions(Connection conn, PePlayer player, Long lastTransactionSave) throws SQLException {
        if (player.getTransactions() == null) {
            player.setTransactions(new ArrayList<>());
        }
        PreparedStatement stm = conn.prepareStatement("SELECT * FROM `transactions` WHERE (`uuid_from` = ? OR `uuid_to` = ?) AND save > ?;");
        stm.setString(1, player.getUuid().toString());
        stm.setString(2, player.getUuid().toString());
        stm.setLong(3, lastTransactionSave);
        ResultSet rs = stm.executeQuery();
        while (rs.next()) {
            String uuidFrom = rs.getString("uuid_from");
            String uuidTo = rs.getString("uuid_to");
            int id = rs.getInt("id");
            Double amount = rs.getDouble("amount");
            Long save = rs.getLong("save");
            Transaction tr = new Transaction(id, uuidFrom, uuidTo, amount, save);
            tr.hasBeenSaved = true;
            player.getTransactions().add(tr);
        }
        PluginMain.LOG.info("Le joueur " + player.getName() + " a " + player.getMoney() + "px.");
    }

    public void loadService() {
        Connection connection = null;
        try {
            connection = DbManager.INSTANCE.openConnection();
            Bukkit.getLogger().info("Chargement des joueurs depuis la base.");
            loadAllPlayers(connection);
        } catch (SQLException throwables) {
            Bukkit.getLogger().severe("Erreur de SQL au démarrage. " + throwables.getLocalizedMessage());
        } finally {
            if (connection != null) {
                try {
                    DbManager.INSTANCE.closeConnection(connection);
                } catch (SQLException throwables) {
                    Bukkit.getLogger().severe("Erreur de déconnexion SQL au démarrage.");
                }
            }
        }
    }

    public void loadAllPePlayerData(Connection conn) throws SQLException {
        //On récupère les derniers items non lus.
        PreparedStatement stm = conn.prepareStatement("SELECT * FROM `player_data` WHERE saveDate > ?;");
        stm.setLong(1, lastLoadSavePePlayerData);
        ResultSet rs = stm.executeQuery();
        Map<UUID, List<PePlayerData>> mapPePlayerData = new HashMap<>();
        while (rs.next()) {
            try {
                UUID playerUUID = UUID.fromString(rs.getString("uuid"));
                PePlayerData pePlayerData = new PePlayerData(playerUUID, PePlayerDataType.valueOf(rs.getString("data_type")), rs.getString("data"), rs.getLong("saveDate"));
                addItemToList(mapPePlayerData, playerUUID, pePlayerData);
                Bukkit.getLogger().info("Donnée de  " + playerUUID + " : " + rs.getString("data_type"));
            } catch (Exception ex) {
                Bukkit.getLogger().severe("§cErreur de lecture des données d'un joueur. " + ex.toString());
            }
        }

        //On sauvegarde les items non saved.
        PreparedStatement stm2 = conn.prepareStatement("INSERT INTO `player_data` (`id`, `uuid`, `data_type`, `data`, `saveDate`) VALUES (NULL, ?, ?, ?, ?);");
        for (PePlayer pePlayer : PlayersManager.INSTANCE.getPlayers()) {
            try {
                List<PePlayerData> pePlayerDataList = pePlayer.getPePlayerData().stream().filter(x -> x.saveDate > lastLoadSavePePlayerData).collect(Collectors.toList());
                for (PePlayerData pePlayerData : pePlayerDataList) {
                    stm2.setString(1, pePlayer.getUuid().toString());
                    stm2.setString(2, pePlayerData.dataType.name());
                    stm2.setString(3, pePlayerData.data);
                    stm2.setLong(4, pePlayerData.saveDate);
                    stm2.addBatch();
                }
            } catch (Exception ex) {
                Bukkit.getLogger().severe("Erreur lors de la sauvegarde des données du joueur " + pePlayer.getName() + " : " + ex.toString());
            }
        }
        stm2.executeBatch();

        //On traite les items non lus.
        for (UUID uuid : mapPePlayerData.keySet()) {
            PePlayer pePlayer = INSTANCE.getPePlayer(uuid);
            mapPePlayerData.get(uuid).forEach(x -> pePlayer.readPePlayerData(x));
        }

        lastLoadSavePePlayerData = System.currentTimeMillis();
    }

    private void addItemToList(Map<UUID, List<PePlayerData>> mapPePlayerData, UUID uuid, PePlayerData pePlayerData) {
        if (!mapPePlayerData.containsKey(uuid)) {
            mapPePlayerData.put(uuid, new ArrayList<>());
        }
        mapPePlayerData.get(uuid).add(pePlayerData);
    }

    public void saveAllModifiedValues() {

    }

    public PePlayer getPePlayer(UUID uuid) {
        try {
            return players.stream().filter(x -> x.getUuid().equals(uuid)).findFirst().get();
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    public void addNewPePlayer(PePlayer pePlayer) {
        if (getPePlayer(pePlayer.getUuid()) == null) {
            players.add(pePlayer);
        } else {
            Bukkit.getLogger().warning("Tentative d'ajout d'un nouveau joueur alors qu'il existe déjà !");
        }
    }
}
