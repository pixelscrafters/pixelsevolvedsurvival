package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

public class TpaCommand extends AbstractCommand {
    public static final String COMMAND_NAME = "tpa";
    public static final TpaCommand INSTANCE = new TpaCommand();

    private TpaCommand() {
        super(COMMAND_NAME);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length == 1) {
                Player p = (Player) sender;
                PePlayer source = PlayersManager.INSTANCE.getPePlayer(p.getUniqueId());
                if (source != null) {
                    Player pDest = Bukkit.getPlayer(args[0]);
                    if (pDest != null && pDest.isOnline()) {
                        PePlayer destination = PlayersManager.INSTANCE.getPePlayer(pDest.getUniqueId());
                        destination.setUuidWaitingForTp(source.getUuid());
                        destination.sendMessage("Le joueur " + source.getName() + " veut se téléporter à vous.");
                        destination.sendMessage("§lfaites /tpy pour §aaccepter §c/tpn pour §4refuser");
                        source.sendMessage("Demande envoyée !");
                    } else {
                        source.sendMessage("Vous ne pouvez pas vous téléporter à un joueur hors ligne ou sur un serveur différent.");
                    }
                }
            } else {
                sender.sendMessage("§dMerci d'y ajouter le nom d'un joueur");
            }
        } else {
            sender.sendMessage("§cLa console ne va pas se téléporter !");
        }
        return true;
    }
}
