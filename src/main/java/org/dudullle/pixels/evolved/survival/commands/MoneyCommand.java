package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.database.PlayersManager;
import org.dudullle.pixels.evolved.survival.money.Transaction;
import org.dudullle.pixels.evolved.survival.players.PePlayer;

import java.util.Map;
import java.util.TreeMap;

public class MoneyCommand extends AbstractCommand {
    public static final String COMMAND_NAME = "money";

    public MoneyCommand() {
        super(COMMAND_NAME);
        Map<String, String> subCommandDescription = new TreeMap<>();
        subCommandDescription.put("", "Affiche votre argent actuel");
        subCommandDescription.put("top", "Affiche le top 10 des joueurs les plus riches");
        subCommandDescription.put("add [MONEY]", "Ajoute de l'argeeeennnttt !");
        this.setSubCommandDescription(subCommandDescription);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (sender instanceof Player) {
                PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(((Player) sender).getUniqueId());
                sender.sendMessage("§bVous avez §d" + pePlayer.getMoney() + "§bpx.");
            } else {
                sender.sendMessage("§cLa console n'a pas d'argent.");
            }
        } else {
            if (args[0].equalsIgnoreCase("top")) {
                sender.sendMessage("§dTop 10");
            } else if (args[0].equalsIgnoreCase("add")) {
                if (sender instanceof Player) {
                    Double amount = 1000.0d;
                    if (args.length == 2) {
                        try {
                            amount = Double.valueOf(args[1]);
                        } catch (NumberFormatException ex) {
                            sender.sendMessage("§cUn nombre est composé de chiffres banane !");
                        }
                    }
                    PePlayer pePlayer = PlayersManager.INSTANCE.getPePlayer(((Player) sender).getUniqueId());
                    Transaction tr = new Transaction(0, "CONSOLE", pePlayer.getUuid().toString(), amount, 0);
                    pePlayer.getTransactions().add(tr);
                }
            } else {
                for (String helpLine : this.getHelpText()) {
                    sender.sendMessage(helpLine);
                }
            }
        }
        return true;
    }
}
