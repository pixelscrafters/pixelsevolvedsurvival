package org.dudullle.pixels.evolved.survival.money;

import org.dudullle.pixels.evolved.survival.database.AbstractDataItemList;

public class Transaction extends AbstractDataItemList {
    private long id;
    private String uuidFrom;
    private String uuidTo;
    private Double amount;
    private long save;

    public Transaction(long id, String uuid_from, String uuid_to, Double amount, long save) {
        this.id = id;
        this.uuidFrom = uuid_from;
        this.uuidTo = uuid_to;
        this.amount = amount;
        this.save = save;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuidFrom() {
        return uuidFrom;
    }

    public void setUuidFrom(String uuidFrom) {
        this.uuidFrom = uuidFrom;
    }

    public String getUuidTo() {
        return uuidTo;
    }

    public void setUuidTo(String uuidTo) {
        this.uuidTo = uuidTo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public long getSave() {
        return save;
    }

    public void setSave(long save) {
        this.save = save;
    }
}
