package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.dudullle.pixels.evolved.survival.permissions.PermissionManager;
import org.dudullle.pixels.evolved.survival.utils.ReturnMessage;

import java.util.Map;
import java.util.TreeMap;

public class PermissionCommand extends AbstractCommand {
    public static final PermissionCommand INSTANCE = new PermissionCommand();
    public static final String COMMAND_NAME = "peperm";

    public PermissionCommand() {
        super(COMMAND_NAME);
        Map<String, String> subCommandDescription = new TreeMap<>();
        subCommandDescription.put("", "Cette aide");
        subCommandDescription.put("reload [all/affects]", "recharge toutes les perms ou réattribue les perms aux joueurs en ligne");
        subCommandDescription.put("addgroup [NAME]", "Ajout d'un groupe de permissions");
        subCommandDescription.put("rmvgroup [NAME]", "Suppression d'un groupe de permissions");
        subCommandDescription.put("addperm [GROUP] [PERM]", "Ajout d'une perm");
        subCommandDescription.put("rmvperm [GROUP] [PERM]", "Suppression d'une perm");
        subCommandDescription.put("addplayer [GROUP] [PLAYER]", "Ajout d'un joueur à un groupe");
        subCommandDescription.put("rmvplayer [GROUP] [PLAYER]", "Suppression d'un joueur d'un groupe");
        subCommandDescription.put("rmvplayer [GROUP] [PLAYER]", "Suppression d'un joueur d'un groupe");
        subCommandDescription.put("list groups", "Liste les groupes");
        this.setSubCommandDescription(subCommandDescription);
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("pe.admin")) {
            return false;
        }
        if (args.length == 0) {
            for (String helpLine : this.getHelpText()) {
                sender.sendMessage(helpLine);
            }
        } else {
            String commandName = args[0];
            String groupName = args.length >= 2 ? args[1] : null;
            String thirdArg = args.length == 3 ? args[2] : null;
            ReturnMessage message = ReturnMessage.SUCCESS;
            switch (commandName) {
                case "addgroup":
                    message = PermissionManager.INSTANCE.addGroup(groupName);
                    break;
                case "rmvgroup":
                    message = PermissionManager.INSTANCE.rmvGroup(groupName);
                    break;
                case "addperm":
                    if (thirdArg != null) {
                        message = PermissionManager.INSTANCE.addPermissionToGroup(groupName, thirdArg);
                    } else {
                        message = ReturnMessage.ERROR_EMPTY;
                    }
                    break;
                case "rmvperm":
                    if (thirdArg != null) {
                        message = PermissionManager.INSTANCE.rmvPermissionToGroup(groupName, thirdArg);
                    } else {
                        message = ReturnMessage.ERROR_EMPTY;
                    }
                    break;
                case "addplayer":
                    if (thirdArg != null) {
                        Player p = Bukkit.getPlayer(thirdArg);
                        if (p != null) {
                            message = PermissionManager.INSTANCE.addPlayerToGroup(groupName, p);
                        } else {
                            message = ReturnMessage.ERROR_EMPTY;
                        }
                    } else {
                        message = ReturnMessage.ERROR_EMPTY;
                    }
                    break;
                case "rmvplayer":
                    if (thirdArg != null) {
                        Player p = Bukkit.getPlayer(thirdArg);
                        if (p != null) {
                            message = PermissionManager.INSTANCE.rmvPlayerToGroup(groupName, p);
                        } else {
                            message = ReturnMessage.ERROR_EMPTY;
                        }
                    } else {
                        message = ReturnMessage.ERROR_EMPTY;
                    }
                    break;
                case "listperms":
                    for (String s : PermissionManager.INSTANCE.getPermsByGroupName(groupName)) {
                        sender.sendMessage("§d-§5 " + s);
                    }
                    break;
                case "reload":
                    if (groupName.equalsIgnoreCase("all")) {
                        PermissionManager.INSTANCE.refresh();
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            PermissionManager.INSTANCE.removeAttachment(p);
                            PermissionManager.INSTANCE.setPermissionsOfPlayer(p);
                        }
                    } else if (groupName.equalsIgnoreCase("affects")) {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            PermissionManager.INSTANCE.removeAttachment(p);
                            PermissionManager.INSTANCE.setPermissionsOfPlayer(p);
                        }
                    }
                    break;
                case "list":
                    for (String s : PermissionManager.INSTANCE.getGroupNames()) {
                        sender.sendMessage("§d-§5 " + s);
                    }
                    break;
            }
            sender.sendMessage("§7RETURN_CODE " + message);
        }
        return true;
    }
}
