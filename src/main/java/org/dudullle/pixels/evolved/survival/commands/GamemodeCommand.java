package org.dudullle.pixels.evolved.survival.commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.TreeMap;

public class GamemodeCommand extends AbstractCommand {
    public static final GamemodeCommand INSTANCE = new GamemodeCommand();
    public static final String COMMAND_NAME = "gamemode";
    private static final GameMode[] gameModes = {GameMode.SURVIVAL, GameMode.CREATIVE, GameMode.ADVENTURE, GameMode.SPECTATOR};

    public GamemodeCommand() {
        super(COMMAND_NAME);
        Map<String, String> subCommandDescription = new TreeMap<>();
        subCommandDescription.put("", "Affiche cette aide");
        subCommandDescription.put("[GAMEMODE]", "Change le gamemode de votre joueur (0 1 2 ou 3)");
        subCommandDescription.put("[GAMEMODE] [PLAYER]", "Change le gamemode du joueur spécifié (1 2 ou 3)");
        this.setSubCommandDescription(subCommandDescription);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            //Commande invalide
            for (String helpLine : this.getHelpText()) {
                sender.sendMessage(helpLine);
            }
        } else {
            String gamemodeString = args[0];
            String playerName = args.length == 2 ? args[1] : null;
            GameMode gm = GameMode.SURVIVAL;
            try {
                Integer gmId = Integer.parseInt(gamemodeString);
                gm = gameModes[gmId];
            } catch (Exception ex) {
                sender.sendMessage("§4Impossible de trouver le gamemode correspondant à " + gamemodeString);
            }
            if (playerName == null && sender instanceof Player && sender.hasPermission("pe.admin.gamemode")) {
                Player p = (Player) sender;
                p.setGameMode(gm);
                p.sendMessage("§dGamemode changé sur §b" + gm.name());
            } else if (playerName != null && sender.hasPermission("pe.admin.gamemode.others")) {
                Player p = Bukkit.getPlayer(playerName);
                if (p != null) {
                    p.setGameMode(gm);
                    p.sendMessage("§dGamemode changé sur §b" + gm.name());
                } else {
                    sender.sendMessage("§cLe joueur spécifié n'a pas été trouvé.");
                }
            } else if (!(sender instanceof Player) && playerName == null) {
                sender.sendMessage("§4Vous n'avez pas la permission pe.admin.gamemode");
            } else if (playerName != null) {
                sender.sendMessage("§4Vous n'avez pas la permission pe.admin.gamemode.others");
            }
        }
        return true;
    }
}
