package org.dudullle.pixels.evolved.survival.enchants;

import org.bukkit.enchantments.Enchantment;
import org.dudullle.pixels.evolved.survival.enchants.enchantments.MinigunEnchant;

import java.lang.reflect.Field;

public class Enchantements {
    public static final Enchantements INSTANCE = new Enchantements();

    public static final AEnchant MINIGUN_ENCHANT = new MinigunEnchant();

    public void registerEnchants() {
        //Ajout des enchants
        try {
            try {
                Field f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.set(null, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Enchantment.registerEnchantment(MINIGUN_ENCHANT);
            } catch (IllegalArgumentException e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
