package org.dudullle.pixels.evolved.survival.alliances;

import org.bukkit.Bukkit;
import org.dudullle.pixels.evolved.survival.PluginMain;
import org.dudullle.pixels.evolved.survival.database.DbManager;
import org.dudullle.pixels.evolved.survival.utils.AlliancePointOfInterest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class AllianceDatabaseManager {
    public static final AllianceDatabaseManager INSTANCE = new AllianceDatabaseManager();
    private final List<AllianceTransaction> transactionsSaved = new ArrayList<>();
    private final Queue<AllianceTransaction> transactionQueueToSave = new LinkedBlockingQueue<>();
    /**
     * Date du dernier chargement des transactions depuis la base (Date juste avant la requête).
     */
    private long lastTransactionLoadTime = 0;
    /**
     * Date du dernier chargement des alliances.
     */
    private long lastAllianceLoadTime = 0;

    public void loadService() {
        //Premier chargement !
        Connection connection = null;
        try {
            connection = DbManager.INSTANCE.openConnection();
            Bukkit.getLogger().info("Chargement des alliances depuis la base.");
            loadAllAlliances(connection);
        } catch (SQLException throwables) {
            Bukkit.getLogger().severe("Erreur de SQL au démarrage. " + throwables.getLocalizedMessage());
        } finally {
            if (connection != null) {
                try {
                    DbManager.INSTANCE.closeConnection(connection);
                } catch (SQLException throwables) {
                    Bukkit.getLogger().severe("Erreur de déconnexion SQL au démarrage.");
                }
            }
        }
    }

    public void loadAllAlliances(Connection conn) throws SQLException {
        Statement stm = conn.createStatement();
        lastAllianceLoadTime = System.currentTimeMillis();
        ResultSet rs = stm.executeQuery("SELECT * FROM alliances");
        while (rs.next()) {
            PeAlliance alliance = new PeAlliance(rs.getString("name"), rs.getLong("save"));
            AllianceManager.INSTANCE.alliances.add(alliance);
            Bukkit.getLogger().info("[PE] Loaded alliance " + alliance.getName());
        }
        //Loading transactions
        stm = conn.createStatement();
        lastTransactionLoadTime = System.currentTimeMillis();
        rs = stm.executeQuery("SELECT * FROM alliance_transactions");
        while (rs.next()) {
            long id = rs.getLong("id");
            long saveDate = rs.getLong("save_date");
            String allianceName = rs.getString("alliance_name");
            AllianceTransaction.DataType dataType = AllianceTransaction.DataType.valueOf(rs.getString("data_type"));
            AllianceTransaction.DataOperation dataOperation = AllianceTransaction.DataOperation.valueOf(rs.getString("operation"));
            String value = rs.getString("value");
            AllianceTransaction allianceTransaction = new AllianceTransaction(id, allianceName, saveDate, dataType, dataOperation, value);
            readAllianceTransaction(allianceTransaction);
        }
    }

    public void refreshFromDatabase() {
        //Récupère toutes les alliances et les transactions
        Bukkit.getScheduler().runTaskAsynchronously(PluginMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                Connection connection = null;
                try {
                    connection = DbManager.INSTANCE.openConnection();
                    List<PeAlliance> alliancesToSave = AllianceManager.INSTANCE.getAlliances().stream().filter(x -> x.saveDate == 0).collect(Collectors.toList());
                    saveNewAlliances(connection, alliancesToSave);
                    saveAllianceTransactions(connection);
                    refreshAlliances(connection);
                } catch (SQLException throwables) {
                    Bukkit.getLogger().severe("Erreur de SQL au démarrage. " + throwables.getLocalizedMessage());
                } finally {
                    if (connection != null) {
                        try {
                            DbManager.INSTANCE.closeConnection(connection);
                        } catch (SQLException throwables) {
                            Bukkit.getLogger().severe("Erreur de déconnexion SQL au démarrage.");
                        }
                    }
                }
            }
        });
    }

    public void addAllianceTransactionToSaveQueue(AllianceTransaction transaction) {
        transactionQueueToSave.add(transaction);
    }

    private void saveAllianceTransactions(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `alliance_transactions` (`id`, `alliance_name`, `save_date`, `data_type`, `value`, `operation`) VALUES (NULL, ?, ?, ?, ?, ?)");
        AllianceTransaction allianceTransaction = transactionQueueToSave.poll();
        while (allianceTransaction != null) {
            preparedStatement.setString(1, allianceTransaction.getAllianceName());
            allianceTransaction.setSaveDate(System.currentTimeMillis());
            preparedStatement.setLong(2, allianceTransaction.getSaveDate());
            preparedStatement.setString(3, allianceTransaction.getDataType().name());
            preparedStatement.setString(4, allianceTransaction.getValue());
            preparedStatement.setString(5, allianceTransaction.getDataOperation().name());
            preparedStatement.addBatch();
            transactionsSaved.add(allianceTransaction);
            allianceTransaction = transactionQueueToSave.poll();
        }
        preparedStatement.executeBatch();
    }

    private void refreshAlliances(Connection conn) throws SQLException {
        PreparedStatement stm = conn.prepareStatement("SELECT * FROM alliances WHERE save >= ?");
        stm.setLong(1, lastAllianceLoadTime);
        lastAllianceLoadTime = System.currentTimeMillis();
        ResultSet rs = stm.executeQuery();
        while (rs.next()) {
            PeAlliance alliance = new PeAlliance(rs.getString("name"), rs.getLong("save"));
            if (AllianceManager.INSTANCE.getByName(alliance.getName()) == null) {
                AllianceManager.INSTANCE.alliances.add(alliance);
                Bukkit.getLogger().info("[PE] Loaded alliance " + alliance.getName());
            } else {
                Bukkit.getLogger().info("[PE] Alliance already loaded " + alliance.getName());
            }
        }
        //Loading transactions
        stm = conn.prepareStatement("SELECT * FROM alliance_transactions WHERE save_date >= ?");
        stm.setLong(1, lastTransactionLoadTime);
        lastTransactionLoadTime = System.currentTimeMillis();
        rs = stm.executeQuery();
        while (rs.next()) {
            long id = rs.getLong("id");
            long saveDate = rs.getLong("save_date");
            String allianceName = rs.getString("alliance_name");
            AllianceTransaction.DataType dataType = AllianceTransaction.DataType.valueOf(rs.getString("data_type"));
            AllianceTransaction.DataOperation dataOperation = AllianceTransaction.DataOperation.valueOf(rs.getString("operation"));
            String value = rs.getString("value");
            AllianceTransaction allianceTransaction = new AllianceTransaction(id, allianceName, saveDate, dataType, dataOperation, value);
            if (!alreadyTreated(allianceTransaction)) {
                readAllianceTransaction(allianceTransaction);
            } else {
                Bukkit.getLogger().warning("Trying to treat one more time the transaction " + allianceTransaction.toString());
            }
        }
    }

    private void saveNewAlliances(Connection connection, List<PeAlliance> peAlliances) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `alliances` (`id`, `name`, `save`) VALUES (NULL, ?, ?)");
        for (PeAlliance alliance : peAlliances) {
            preparedStatement.setString(1, alliance.getName());
            alliance.saveDate = System.currentTimeMillis();
            preparedStatement.setLong(2, alliance.saveDate);
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }

    private Boolean alreadyTreated(AllianceTransaction transaction) {
        try {
            List<AllianceTransaction> optionalAllianceTransaction = transactionsSaved.stream().filter(x -> x != null && (x.getSaveDate() == transaction.getSaveDate()
                    && x.getAllianceName().equals(transaction.getAllianceName())
                    && x.getDataType().equals(transaction.getDataType()) && x.getDataOperation().equals(transaction.getDataOperation())
                    && x.getValue().equals(transaction.getValue()))).collect(Collectors.toList());
            return !optionalAllianceTransaction.isEmpty();
        } catch (Exception ex) {
            Bukkit.getLogger().severe("[PE] ERROR " + transaction.toString());
            return false;
        }
    }

    private void readAllianceTransaction(AllianceTransaction transaction) {
        PeAlliance peAlliance = AllianceManager.INSTANCE.getByName(transaction.getAllianceName());
        if (peAlliance == null) {
            Bukkit.getLogger().severe("[PE] Trying to load transaction of an unknown alliance. Maybe later it will work." + transaction.toString());
            return;
        } else {
            try {
                AllianceTransaction.DataOperation operation = transaction.getDataOperation();
                switch (transaction.getDataType()) {
                    case FLAG:
                        PeAllianceFlag flag = PeAllianceFlag.fromJson(transaction.getValue());
                        if (operation == AllianceTransaction.DataOperation.ADD) {
                            peAlliance.addFlag(flag, true);
                        } else {
                            peAlliance.removeFlag(flag, true);
                        }
                        break;
                    case ADMIN:
                        if (operation == AllianceTransaction.DataOperation.ADD) {
                            peAlliance.admins.add(UUID.fromString(transaction.getValue()));
                        } else {
                            peAlliance.admins.remove(UUID.fromString(transaction.getValue()));
                        }
                        break;
                    case MEMBER:
                        //TODO Dans le cas d'un membre présent dans deux factions, on garde uniquement la faction où il est admin, sinon on le retire de toutes les factions avec un gentil message
                        if (operation == AllianceTransaction.DataOperation.ADD) {
                            peAlliance.members.add(UUID.fromString(transaction.getValue()));
                        } else {
                            peAlliance.members.remove(UUID.fromString(transaction.getValue()));
                        }
                        break;
                    case WAITING_MEMBER:
                        if (operation == AllianceTransaction.DataOperation.ADD) {
                            peAlliance.waitingApproval.add(UUID.fromString(transaction.getValue()));
                        } else {
                            peAlliance.waitingApproval.remove(UUID.fromString(transaction.getValue()));
                        }
                        break;
                    case HOME:
                        if (operation == AllianceTransaction.DataOperation.SET) {
                            AlliancePointOfInterest alliancePointOfInterest = AlliancePointOfInterest.fromJson(transaction.getValue());
                            peAlliance.setHome(alliancePointOfInterest);
                        }
                        break;
                    case ALL:
                        if (operation == AllianceTransaction.DataOperation.RMV) {
                            peAlliance.deleteAlliance(null, true);
                        }
                }
                Bukkit.getLogger().info("[PE] Alliance transaction " + transaction.toString());
                transactionsSaved.add(transaction);
            } catch (Exception ex) {
                Bukkit.getLogger().severe("[PE] ERROR ALLIANCE TRANSACTION " + transaction.toString());
                ex.printStackTrace();
            }

        }
    }
}
